//===========================================================================
// Two-particle correlation analysis
//
// Functions for creating pretty figures
//
// 2016: Renata Kopecna
//===========================================================================

//--- c++ libs ---//
#include <string.h>
#include <iostream>
#include <sstream>

//--- ROOT libs ---//
#include <TROOT.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TColor.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TF1.h>
#include <TStyle.h>
#include <TPaveText.h>
#include <TLine.h>
#include <TEfficiency.h>
#include <TLegend.h>

using namespace std;


///////////////////////////////////////////////////////////////////////////////////
///
///
///   ##### Basic rules for plotting various histograms
///
///     ### design_markers
///         # Designs histogram's markers, sets titles and color
///
///     ### design_lines
///         # Designs histogram's lines, sets titles and color
///
///     ### design_filled_plot
///         # Designs filled histograms, including titles, color and fill style
///         # two options: with or without lines
///
///     ### design_canvas, design canvas TH2F, design_canvas_ridge_fancy
///         # initializes canvases basic, TH2F and fancy canvases
///
///     ### design_2D_legend, design_2D_legend_integrated
///         # designes box with legend with given pT and multiplicity
///
///     ### design_TH2F
///         # Sets axis names and palette for 2D histograms
///
///     ### design_TH2F_ratio
///         # Sets axis names and palette for 2D histograms for ratios for misereconstructed PVs
///
///     ### design_function
///         # Sets width and color of a TF1
///
///     ### plot_title
///         # Plots titles for given pT and multiplicity bins
///
///     ### name_nVELO_bin
///         # Returns string with the multiplicity boundaries
///
///     ### design_multiplicity, design_multiplicity_preliminary
///         # Designs multiplicity plots
///
///     ### design_ridge, design_ridge_fancy
///         # Designs ridge plots
///
///     ### design_cut_line
///         # Designes the line used for cuts in distribution plots
///
///     ### design_ratio_line
///         # Designes the line for True/MC ratios
///
///
////////////////////////////////////////////////////////////////////////////////////

void design_markers(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    histogram->SetMarkerStyle(8);
    histogram->SetMarkerColor(color);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.9);
    histogram->GetXaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.045);
    histogram->GetYaxis()->SetLabelSize(0.045);

    histogram->SetLineColor(color);
}
void design_markers(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    histogram->SetMarkerStyle(8);
    histogram->SetMarkerColor(color);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.9);
    histogram->GetXaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.045);
    histogram->GetYaxis()->SetLabelSize(0.045);

    histogram->SetLineColor(color);
}
void design_markers(TEfficiency *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    histogram->SetMarkerStyle(8);
    histogram->SetMarkerColor(color);
    histogram->SetLineColor(color);
}

void design_lines(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.5);
    histogram->GetXaxis()->SetTitleOffset(1.0);

    histogram->GetYaxis()->SetTitleSize(0.04);
    histogram->GetXaxis()->SetTitleSize(0.04);
    histogram->GetYaxis()->SetLabelSize(0.04);
    histogram->GetXaxis()->SetLabelSize(0.04);

    histogram->SetLineColor(color);
}
void design_lines(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.5);
    histogram->GetXaxis()->SetTitleOffset(1.0);

    histogram->GetYaxis()->SetTitleSize(0.04);
    histogram->GetXaxis()->SetTitleSize(0.04);
    histogram->GetYaxis()->SetLabelSize(0.04);
    histogram->GetXaxis()->SetLabelSize(0.04);

    histogram->SetLineColor(color);
}

void design_filled_plot(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, int fill_style){
    gStyle->SetOptStat(0);

    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleOffset(1.0);

    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.05);

    histogram->SetFillColor(fill_color);
    histogram->SetFillStyle(fill_style);
}
void design_filled_plot(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, int fill_style){
    gStyle->SetOptStat(0);

    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleOffset(1.0);

    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.05);

    histogram->SetFillColor(fill_color);
    histogram->SetFillStyle(fill_style);
}
void design_filled_plot(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, Color_t line_color, int fill_style){
    gStyle->SetOptStat(0);

    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleOffset(1.0);


    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.05);

    histogram->SetLineColor(line_color);
    histogram->SetFillColor(fill_color);
    histogram->SetFillStyle(fill_style);
}
void design_filled_plot(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, Color_t line_color, int fill_style){
    gStyle->SetOptStat(0);

    histogram->SetLineWidth(2);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    histogram->GetYaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetTitleOffset(1.0);


    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.05);
    histogram->GetXaxis()->SetLabelSize(0.05);

    histogram->SetLineColor(line_color);
    histogram->SetFillColor(fill_color);
    histogram->SetFillStyle(fill_style);
}

void design_canvas(TCanvas *canvas){
    canvas->SetRightMargin(0.04);
    canvas->SetTopMargin(0.04);
    canvas->SetLeftMargin(0.18);
    canvas->SetBottomMargin(0.12);
}
void design_canvas_TH2F(TCanvas *canvas){
    canvas->SetRightMargin(0.15);
    canvas->SetTopMargin(0.04);
    canvas->SetBottomMargin(0.1);
    canvas->SetLeftMargin(0.1);
}
void design_canvas_ridge_fancy(TCanvas *canvas){
    canvas->SetRightMargin(0.515);
    canvas->SetTopMargin(0.05);
    canvas->SetBottomMargin(0.12);
    canvas->SetLeftMargin(2.45);
}


void design_TH2F(TH2F *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette){
    gStyle->SetOptStat(0);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    string zaxis_name_offset = "                                                             " + zaxis_name;
        //#IfItWorksItAintStupid
    histogram->GetZaxis()->SetTitle(zaxis_name_offset.c_str());

    histogram->GetXaxis()->SetTitleOffset(1.5);
    histogram->GetYaxis()->SetTitleOffset(1.5);
    histogram->GetZaxis()->SetTitleOffset(1.0);
    histogram->GetZaxis()->SetLabelSize(0.03);
    histogram->GetZaxis()->CenterTitle(kTRUE);
    histogram->SetTitle("");
    gStyle->SetPalette(palette);
}
void design_TH2F(TH2D *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette){
    gStyle->SetOptStat(0);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    string zaxis_name_offset = "                                                             " + zaxis_name;
    histogram->GetZaxis()->SetTitle(zaxis_name_offset.c_str());

    histogram->GetXaxis()->SetTitleOffset(1.5);
    histogram->GetYaxis()->SetTitleOffset(1.5);
    histogram->GetZaxis()->SetTitleOffset(1.0);

    histogram->GetZaxis()->SetLabelSize(0.03);
    histogram->GetZaxis()->CenterTitle(kTRUE);
    histogram->SetTitle("");
    gStyle->SetPalette(palette);
}

void design_TH2F_ratio(TH2F *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette){
    gStyle->SetOptStat(0);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    string zaxis_name_offset = "        " + zaxis_name;
    histogram->GetZaxis()->SetTitle(zaxis_name_offset.c_str());

    histogram->GetXaxis()->SetTitleOffset(1.2);
    histogram->GetYaxis()->SetTitleOffset(1.2);
    histogram->GetZaxis()->SetTitleOffset(1.2);
    histogram->GetZaxis()->SetLabelSize(0.03);
    histogram->GetZaxis()->CenterTitle(kTRUE);
    histogram->SetTitle("");
    gStyle->SetPalette(palette);
}
void design_TH2F_ratio(TH2D *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette){
    gStyle->SetOptStat(0);
    histogram->GetXaxis()->SetTitle(xaxis_name.c_str());
    histogram->GetYaxis()->SetTitle(yaxis_name.c_str());
    string zaxis_name_offset = "        " + zaxis_name;
    histogram->GetZaxis()->SetTitle(zaxis_name_offset.c_str());

    histogram->GetXaxis()->SetTitleOffset(1.2);
    histogram->GetYaxis()->SetTitleOffset(1.2);
    histogram->GetZaxis()->SetTitleOffset(1.2);
    histogram->GetZaxis()->SetLabelSize(0.03);
    histogram->GetZaxis()->CenterTitle(kTRUE);
    histogram->SetTitle("");
    gStyle->SetPalette(palette);
}

void design_function(TF1 *graph, Color_t color){
    gStyle->SetOptStat(0);
    graph->SetLineWidth(3);
    graph->SetLineColor(color);

}
void design_function(TF1 *graph, Color_t color, int style){
    gStyle->SetOptStat(0);
    graph->SetLineWidth(3);
    graph->SetLineStyle(style);
    graph->SetLineColor(color);

}

void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color){
    gStyle->SetOptStat(0);
    design_markers(histogram, xaxis_name, yaxis_name, color);
    histogram->SetMarkerSize(3);
    histogram->GetXaxis()->SetTitleOffset(1.0);
    histogram->GetYaxis()->SetTitleOffset(1.6);
    histogram->SetTitle("");
    histogram->GetXaxis()->SetLabelSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.04);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetTitleSize(0.05);
}
void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color, int marker_style){
    gStyle->SetOptStat(0);
    design_markers(histogram, xaxis_name, yaxis_name, color);
    histogram->GetXaxis()->SetTitleOffset(1.0);
    histogram->GetYaxis()->SetTitleOffset(1.6);
    histogram->SetMarkerSize(3);
    histogram->SetMarkerStyle(marker_style);
    histogram->SetTitle("");
    histogram->GetXaxis()->SetLabelSize(0.05);
    histogram->GetYaxis()->SetLabelSize(0.04);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetTitleSize(0.05);
}
void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color, int marker_style, double x_low, double x_up, double y_low, double y_up){
    gStyle->SetOptStat(0);
    design_markers(histogram, xaxis_name, yaxis_name, color);
    histogram->SetMarkerSize(3);
    histogram->SetMarkerStyle(marker_style);
    histogram->SetTitle("");

    histogram->GetXaxis()->SetLabelSize(0.05);
    histogram->GetXaxis()->SetTitleSize(0.05);
    histogram->GetXaxis()->SetTitleOffset(1.0);
    histogram->GetXaxis()->SetRangeUser(x_low, x_up);


    histogram->GetYaxis()->SetTitleOffset(1.6);
    histogram->GetYaxis()->SetLabelSize(0.04);
    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetTitleSize(0.05);
    histogram->GetYaxis()->SetRangeUser(y_low, y_up);

}

TLine *design_cut_line(float x1, float y1, float x2, float y2, Color_t color){
    TLine *line = new TLine(x1,y1,x2,y2);
    line->SetLineColor(color);
    line->SetLineWidth(5);
    return line;
}
TLine *design_ratio_line(float x1, float x2, Color_t color){
    TLine *line = new TLine(x1,1,x2,1);
    line->SetLineColor(color);
    line->SetLineWidth(3);
    line->SetLineStyle(2);
    return line;
}

