#include "TColor.h"
#include <TStyle.h>

#ifndef COLORS_HH
#define COLORS_HH

//=========================================
//
//          Colors
//
//=========================================

namespace myColorScheme{
    /* // Taken from https://gist.github.com/gipert/df72b67c1d02bbb41f1dd406b6397811
       * Handy enum for later color referencing
       */
      enum {
          // Bright color scheme
          kTBriBlue        = 9000,
          kTBriCyan        = 9001,
          kTBriGreen       = 9002,
          kTBriYellow      = 9003,
          kTBriRed         = 9004,
          kTBriPurple      = 9005,
          kTBriGrey        = 9006,

          // Vibrant color scheme
          kTVibBlue        = 9007,
          kTVibCyan        = 9008,
          kTVibTeal        = 9009,
          kTVibOrange      = 9010,
          kTVibRed         = 9011,
          kTVibMagenta     = 9012,
          kTVibGrey        = 9013,

          // Muted color scheme //10 colors
          kTMutIndigo      = 9014,
          kTMutCyan        = 9015,
          kTMutTeal        = 9016,
          kTMutGreen       = 9017,
          kTMutOlive       = 9018,
          kTMutSand        = 9019,
          kTMutRose        = 9020,
          kTMutWine        = 9021,
          kTMutPurple      = 9022,
          kTMutPaleGrey    = 9023,

          // Light color scheme //9 colors
          kTLigLightBlue   = 9024,
          kTLigLightCyan   = 9025,
          kTLigMint        = 9026,
          kTLigPear        = 9027,
          kTLigOlive       = 9028,
          kTLigLightYellow = 9029,
          kTLigOrange      = 9030,
          kTLigPink        = 9031,
          kTLigPaleGrey    = 9032,

          // To label "bad" data (see BuRd and PRGn palettes below)
          kTBadData        = 9033


      };

      void init() {
              gStyle->SetColorModelPS(0);

              // Bright color scheme
              new TColor(kTBriBlue,         68./255, 119./255, 170./255, "tol-bri-blue"       );
              new TColor(kTBriCyan,        102./255, 204./255, 238./255, "tol-bri-cyan"       );
              new TColor(kTBriGreen,        34./255, 136./255,  51./255, "tol-bri-green"      );
              new TColor(kTBriYellow,      204./255, 187./255,  68./255, "tol-bri-yellow"     );
              new TColor(kTBriRed,         238./255, 102./255, 119./255, "tol-bri-red"        );
              new TColor(kTBriPurple,      170./255,  51./255, 119./255, "tol-bri-purple"     );
              new TColor(kTBriGrey,        187./255, 187./255, 187./255, "tol-bri-grey"       );

              // Vibrant color scheme
              new TColor(kTVibBlue,          0./255, 119./255, 187./255, "tol-vib-blue"       );
              new TColor(kTVibCyan,         51./255, 187./255, 238./255, "tol-vib-cyan"       );
              new TColor(kTVibTeal,          0./255, 153./255, 136./255, "tol-vib-teal"       );
              new TColor(kTVibOrange,      238./255, 119./255,  51./255, "tol-vib-orange"     );
              new TColor(kTVibRed,         204./255,  51./255,  17./255, "tol-vib-red"        );
              new TColor(kTVibMagenta,     238./255,  51./255, 119./255, "tol-vib-magenta"    );
              new TColor(kTVibGrey,        187./255, 187./255, 187./255, "tol-vib-grey"       );

              // Muted color scheme
              new TColor(kTMutIndigo,       51./255,  34./255, 136./255, "tol-mut-indigo"     );
              new TColor(kTMutCyan,        136./255, 204./255, 238./255, "tol-mut-cyan"       );
              new TColor(kTMutTeal,         68./255, 170./255, 153./255, "tol-mut-teal"       );
              new TColor(kTMutGreen,        17./255, 119./255,  51./255, "tol-mut-green"      );
              new TColor(kTMutOlive,       153./255, 153./255,  51./255, "tol-mut-olive"      );
              new TColor(kTMutSand,        221./255, 204./255, 119./255, "tol-mut-sand"       );
              new TColor(kTMutRose,        204./255, 102./255, 119./255, "tol-mut-rose"       );
              new TColor(kTMutWine,        136./255,  34./255,  85./255, "tol-mut-wine"       );
              new TColor(kTMutPurple,      170./255,  68./255, 153./255, "tol-mut-purple"     );
              new TColor(kTMutPaleGrey,    221./255, 221./255, 221./255, "tol-mut-palegrey"   );

              // Light color scheme
              new TColor(kTLigLightBlue,   119./255, 170./255, 221./255, "tol-lig-lightblue"  );
              new TColor(kTLigLightCyan,   153./255, 221./255, 255./255, "tol-lig-lightcyan"  );
              new TColor(kTLigMint,         68./255, 187./255, 153./255, "tol-lig-mint"       );
              new TColor(kTLigPear,        187./255, 204./255,  51./255, "tol-lig-pear"       );
              new TColor(kTLigOlive,       170./255, 170./255,   0./255, "tol-lig-olive"      );
              new TColor(kTLigLightYellow, 238./255, 221./255, 136./255, "tol-lig-lightyellow");
              new TColor(kTLigOrange,      238./255, 136./255, 102./255, "tol-lig-orange"     );
              new TColor(kTLigPink,        255./255, 170./255, 187./255, "tol-lig-pink"       );
              new TColor(kTLigPaleGrey,    221./255, 221./255, 221./255, "tol-lig-palegrey"   );

              new TColor(kTBadData,        255./255, 238./255, 153./255, "tol-bad-data"       );
          }
};

#endif // COLORS_HH
