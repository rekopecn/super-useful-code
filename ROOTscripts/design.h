 #ifndef DESIGN_H
#define DESIGN_H


//--- ROOT libraries ---//
#include <TROOT.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLine.h>
#include <TF1.h>
#include <TEfficiency.h>

#include <TPaveText.h>

using namespace std;

void design_markers(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color);
void design_markers(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t color);
void design_markers(TEfficiency *histogram, string xaxis_name, string yaxis_name, Color_t color);
    //sets plotting attributes to histograms with markers

void design_lines(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color);
void design_lines(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t color);
    //sets plotting attributes to histogram with lines

void design_filled_plot(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, Color_t line_color, int fill_style);
void design_filled_plot(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, Color_t line_color, int fill_style);
void design_filled_plot(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, int fill_style);
void design_filled_plot(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t fill_color, int fill_style);
    //sets plotting attributes to filled histogram

void design_canvas(TCanvas *canvas);
void design_canvas_TH2F(TCanvas *canvas);
void design_canvas_ridge_fancy(TCanvas *canvas);
TObjArray *makePadsHists(const Int_t npx, const Int_t npy, const Float_t tm, const Float_t lm, TCanvas *canv = NULL, const Char_t *name = "");
    //sets basic attributes to canvas

void design_function(TF1 *graph, Color_t color);
void design_function(TF1 *graph, Color_t color, int style);
    //sets basic attributes to graph

void design_TH2F(TH2F *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette );
void design_TH2F(TH2D *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette );
void design_TH2F_ratio(TH2F *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette);
void design_TH2F_ratio(TH2D *histogram, string xaxis_name, string yaxis_name, string zaxis_name, int palette);
     //sets plotting attributes to 2D histograms


void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color);
void design_TH1F(TH1D *histogram, string xaxis_name, string yaxis_name, Color_t color);
void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color, int marker_style);
void design_TH1F(TH1F *histogram, string xaxis_name, string yaxis_name, Color_t color, int marker_style, double x_low, double x_up, double y_low, double y_up);
    //sets basic attributes for ridge plots


TLine *design_cut_line(float x1, float y1, float x2, float y2,Color_t color);
    //sets basic attributes for ridge plots

TLine *design_ratio_line(float x1, float x2, Color_t color);


#endif // DESIGN_H
