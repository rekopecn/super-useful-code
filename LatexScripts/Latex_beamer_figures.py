from __future__ import print_function

def make_empty_frames_with_figures(fr, to, FigName, FigType, OutName):
    f = open(OutName,'w')
    for num in range (fr, to+1):
        print("\\begin{frame}{}",file = f)
        print("",file = f)
        print("\t\\begin{figure}",file = f)
        print("\t\t\\centering",file = f)
        print("\t\t\\includegraphics[width=0.99\\textwidth]{./figures/" + FigName + "_%i." %(num) + FigType + "}" ,file = f)
        print("\t\\end{figure}",file = f)
        print("",file = f)
        print("\\end{frame}",file = f)
        print("",file = f)
        print("%=======================================%",file = f)
        print("%=======================================%",file = f)
        print("",file = f)
    f.close()

def print_figures_steps(fr, to, step, FigName, FigType, OutName):
    import numpy as np

    loopover = np.linspace(fr,to,int((to-fr)/step), endpoint=False)    
    f = open(OutName,'w')
    for num in loopover:
        print("\t\t\\includegraphics[width=0.24\\textwidth]{./figures/" + FigName + "_%f." %(num) + FigType + "}" ,file = f)
        print("",file = f)
    f.close()
    
def print_figures_steps_simple(fr, to, step):    
    for num in range(943,1000):
        if ((num+1)%4==0): print("\end{frame}")
        if ((num+1)%4==0): print("\\begin{frame}")
        if ((num+2)%2==0): string_tmp = "\\\\"
        else: string_tmp = ""
        print("\t\t\\includgraphics[width=0.24\\textwidth]{figures/MassFits/Run1_KplusPi0Resolved_JpsiOnly_BplusMassModel_OneCB_SingleExponential_DTF_removedMultiple_TMVA0."+"%f." %(num)+"999_constrained_fixShape_fixedMassWindow.eps }"+string_tmp)
    return        
         
def read_ls(path):
    import subprocess
    from subprocess import Popen, PIPE

    command = subprocess.Popen('ls '+path, shell=True, stdout=subprocess.PIPE)	
    stdout = command.communicate()[0] #gives the return of the ls, second member of the list is not important
    return stdout.decode().split('\n') #splits the string into a list of files that are in the folder    

def print_figures_from_ls(path):
    for count, fig in enumerate(read_ls(path),1):
        print("\\only<%i>{\\includegraphics[width=0.99\\textwidth]{%s}}" % (count,fig))
    return    
def print_figures_from_ls_plus(path):
    for fig in read_ls(path):
        print("\\only<+>{\\includegraphics[width=0.99\\textwidth]{%s}\\addtocounter{framenumber}{1}}" % (fig))
    return    

print_figures_from_ls("../plots/All/")