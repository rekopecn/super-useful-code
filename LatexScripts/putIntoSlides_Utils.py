from __future__ import print_function
import numpy as np

def RunTag(Run):
    return "Run"+str(Run)

def RunTagEff(Run,RunEff, onlyAngles):
    onlyAngTag = "_OnlyAngles" if onlyAngles else ""
    if (Run==-1): return onlyAngTag +"_AllPDFs"
    if (Run == RunEff):  
        if (Run == "1"): return "_OnlyRun1"
        if (Run == "2"): return "_OnlyRun2"
        else:          return "_" + RunTag(Run) + onlyAngTag + "_AllPDFs"    
    else:              return "_Run"+(RunEff)

def RunList():
    return ["1","2","12"]
def RunDict():
    return{ "1" : ["1"],
            "2" : ["2"],
            "12": ["1","2"]
    }

def binTag(nBins):
    if   (nBins==1):  return "_1BIN"
    elif (nBins==4):  return "_ALLBINS"
    else:             return "_"+str(nBins)+"BINS"


def binTagFull(nBins, bin):
    return binTag(nBins) + "_bin"+str(bin)

def binTitle(nBins,bin):
    return str(bin+1)+"/" + str(nBins) + " bin" + ("" if nBins==1 else "s")

def rareJpsiTag(isRef):
    if (isRef): return "_Jpsi"
    else:       return "_Signal"

def foldTag(fold):
    if (fold == -1): return ""    
    else:            return "_folding"+str(fold)

def toyTag(toy):
    if (toy): return"_FinalToys"
    else: return ""

# # # # # # # # # # # # # # # # # # # 
#                                   #
#   Main part of the plot maker!    #
#                                   #
# # # # # # # # # # # # # # # # # # #
def make_figure_slides(folder, mainName, listOfEntries, defaultTitle, figsPerSlide, figsPerRow, figType, outputFile):
    
    #Get width of the pic based on the number of plots per row
    picWidth = 0.95/figsPerRow    
    
    #Pass in the mainName of the figure with __LOOP__ where the loop should happen
    lineList = []
    for num,x in enumerate(listOfEntries,start =1):
        name = mainName.replace("__LOOP__",x,1)
        newLine = "\\\\" if (num%figsPerRow==0) else "" #Put // at the end of each row
        lineList.append("\t\\includegraphics[width=" + "%.2f" %picWidth  +"\\textwidth]{" + folder + name + figType + "}"+newLine)

    #Open the file
    f = open(outputFile,'w')
    for num,line in enumerate(lineList, start = 1):
        if ((num-1)%figsPerSlide==0): #Print the begining of a slide
            print("\\begin{frame}{"+defaultTitle+"}",file = f) #TODO: Replace the list of titles by something sensible
            print("",file = f)
            print("\\centering",file = f)
        print(line, file =f) #Print the includegraphics
        if (num%figsPerSlide==0):  #Print the end of a slide
            print("\\end{frame}",file = f)
            print("%-----------------------------------------",file = f)
            print("",file = f)           
    if (len(lineList)%figsPerSlide !=0):
        print("\\end{frame}",file = f)
    f.close()
    
    #Drop everything after "." in the file name and print
    print("\\include{"+outputFile.split(".",1)[0]+"}")