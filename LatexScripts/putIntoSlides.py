from __future__ import print_function
import numpy as np 

from putIntoSlides_Utils import *


FIG_FOLDER = "/home/renata/B2KMuMu/sigma0/B2KstarMuMu/code/ewp-Bplus2Kstmumu-AngAna/FCNCfitter/plots/"
MASS_FIG_FOLDER = FIG_FOLDER + "MassFit/"
MC_FIG_FOLDER = FIG_FOLDER + "MCfit/"
TOYS_FIG_FOLDER = FIG_FOLDER + "Toys/"
MAIN_FIG_FOLDER = FIG_FOLDER + "MainFit/"
BKG_FIG_FOLDER = FIG_FOLDER + "bkgFit/"
GENLVL_FIG_FOLDER = FIG_FOLDER + "GenLvlFit/"

TEX_FILE_FOLDER = "includes/" #Don't use ./ as it efs up the print of includes

#FIG_FOLDER  = "./figures"

OUTPUT_TAG = ""
TITLE_TAG = ""

decName = "KplusPi0Resolved"
angles = ["ctk", "ctl", "phi"]
mass_plots = ["m_log","m"]#, "mkpi"]
ang_params  = ["Fl", "S3", "S4", "S5", "Afb", "S7", "S8", "S9"]
mass_params = ["m_b", "m_sigma_1", "alpha_1", "alpha_2", "n_1", "n_2"]
mass_bkg = ["m_lambda","f_sig"]
angle_bkg = ["cbkgctk1","cbkgctk2","cbkgctk3","cbkgctl1","cbkgctl2"] 



def projections(angle,Run): #Plot all angle projections in 18 bins
    listOfEntries = list(map(str,np.arange(0,18,1)))
    defaultTitle = angle + " projections, " + RunTag(Run)
    outputName = angle +  "proj_" + RunTag(Run)
    figsPerSlide=6
    figsPerRow=3
    make_figure_slides(FIG_FOLDER+"/angular/",angle+"eff__LOOP___" + decName + "_" + RunTag(Run), listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", outputName+".tex")


def massFit(nBins, observable, Run): #Plot mass-only fits
    massFitName = "_massfit" + rareJpsiTag(False) 
    path = observable + massFitName + "Fit" + binTag(nBins) + "_bin__LOOP___"+ RunTag(Run) + ""
    figsPerSlide=6
    figsPerRow=3
    
    make_figure_slides(MASS_FIG_FOLDER,path,
                       list(map(str,np.arange(0,nBins,1))),"Signal mass fit",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MassFit_Signal_"+observable+"_"+RunTag(Run)+".tex")
    return 

def massFitRef(Run): 
    massFitName = "_massfit" + rareJpsiTag(True) 
    path = "__LOOP__" + massFitName + "Fit" + binTag(1) + "_bin0_" + RunTag(Run) + ""
    figsPerSlide=2
    figsPerRow=2
    
    make_figure_slides(MASS_FIG_FOLDER,path,
                       mass_plots,"Reference mass fit",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MassFit_Jpsi_"+RunTag(Run)+".tex")
    return 

def massFitMCRef(Run): #Plot only mass fit of reference MC 
    MCfitName = "_MC" + rareJpsiTag(True) + "Fit"
    path = "__LOOP__" + MCfitName + binTag(1) + "_bin0_SimultaneousFit_" + RunTag(Run) + "_OnlyMass"
    figsPerSlide=2
    figsPerRow=2
    
    make_figure_slides(MC_FIG_FOLDER + "Jpsi/",path,
                       mass_plots,"MC Reference mass fit",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MC_MassFit_Jpsi_"+RunTag(Run)+".tex")
    return 
    
def massFitMC(nBins, observable, Run): 
    MCfitName = "_MC" + rareJpsiTag(False) + "Fit"
    path = observable + MCfitName + binTag(nBins) + "_bin__LOOP___SimultaneousFit_" + RunTag(Run) 
    figsPerSlide=5
    figsPerRow=3
    
    make_figure_slides(MC_FIG_FOLDER + "Signal/",path,
                       list(map(str,np.arange(0,nBins,1))),"MC Signal mass fit",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MC_MassFit_Signal_"+observable+"_"+RunTag(Run)+".tex")
    return 

def angleFitMC(nBins, bin, Run): #Plot only angles fit of signal MC
    MCfitName = "_MC" + rareJpsiTag(False) + "Fit"
    path = "__LOOP__" + MCfitName + binTagFull(nBins,bin)+"_SimultaneousFit_" + RunTag(Run) + "_OnlyAngles_AllPDFs"
    figsPerSlide=3
    figsPerRow=3
    
    make_figure_slides(MC_FIG_FOLDER + "Signal/",path,
                       angles,"MC Signal fit, " + binTitle(nBins,bin),
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MC_Signal"+binTagFull(nBins,bin)+"_"+RunTag(Run)+"_onlyAngles.tex")
    return 

def MainFitRef(Run, Toy, fold): 
    fitName = rareJpsiTag(True) + "Fit"
    path = "__LOOP__" + fitName + toyTag(Toy)+ binTag(1) + "_bin"+str(0)+"_SimultaneousFit" + foldTag(fold) + "_" + RunTag(Run) + "_AllPDFs"
    figsPerSlide=6
    figsPerRow=3

    title = "Main fit, Ref"+ (", toy" if Toy else "")
    if (fold != -1): title += ", fld " + str(fold)
    
    make_figure_slides(MAIN_FIG_FOLDER, path,
                       angles+mass_plots,title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MainFit_Ref"+toyTag(Toy)+ foldTag(fold)+ "_"+RunTag(Run)+".tex")
    return 

def MainFit(Run, nBins, bin, fold): 
    fitName =  rareJpsiTag(False) + "Fit"
    path = "__LOOP__" + fitName + binTagFull(nBins,bin) +"_SimultaneousFit" + foldTag(fold) +"_" + RunTag(Run) + "_AllPDFs"
    figsPerSlide=6
    figsPerRow=3

    title = "Main fit, toy, "+str(bin+1)+"/" + str(nBins) + " bin(s)"
    if (fold != -1): title += ", fld " + str(fold)
    
    make_figure_slides(MAIN_FIG_FOLDER, path,
                       angles+mass_plots, title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MainFit"+ binTagFull(nBins,bin) + foldTag(fold)+"_"+RunTag(Run)+".tex")
    return 

def ToyFitCheckRef():
    path = "Init_vs_Fit___LOOP___Ref"
    plot_list = ang_params + angle_bkg +mass_params + mass_bkg 
    figsPerSlide=6
    figsPerRow=3

    make_figure_slides("./figures/FullFitToy/",path,
                       plot_list,"Toy init vs fit, Reference-like",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"ToyInitFit.tex")
    return 

def MainFitToyCheck(isRef):
    refTag = "_Ref" if isRef else ""
    path = "FinalToy_Init_vs_Fit___LOOP__" + refTag
    plot_list = ang_params + angle_bkg +mass_params + mass_bkg 
    figsPerSlide=6
    figsPerRow=3

    make_figure_slides(TOYS_FIG_FOLDER,path,
                       plot_list,"Main fit vs init," + " Reference-like" if isRef else " Signal-like",
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MainFitToy"+ refTag+".tex")
    return 

def MCfit(nBins, bin, Run, fold): #Plot MC fit everything
    MCfitName = "_MC" + rareJpsiTag(False) + "Fit"
    
    path = "__LOOP__" + MCfitName + binTagFull(nBins,bin)+"_SimultaneousFit" + foldTag(fold) +"_" + RunTag(Run) + "_AllPDFs"
    figsPerSlide=6
    figsPerRow=3
    
    title = "MC Signal fit, "+binTitle(nBins,bin)
    if (fold != -1): title += ", fld " + str(fold)
    make_figure_slides(MC_FIG_FOLDER + "Signal/",path,
                       angles+mass_plots, title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"MC_Signal"+binTagFull(nBins,bin)+ foldTag(fold)+"_"+RunTag(Run)+".tex")
    return 

def BkgFitRef(Run, sideband): 

    var_list = ["ctk", "ctl", "phi", "mkpi"]

    massFitName = "_Bckgnd" + rareJpsiTag(True) 
    sidebandTag = ""
    if (sideband ==-1):  sidebandTag = "_LowBmass"
    elif (sideband ==0): sidebandTag = "_HighBmass"

    sidebandTitle = ""
    if (sideband ==-1):  sidebandTitle = ", lower sideband"
    elif (sideband ==0): sidebandTitle = ", upper sideband"


    path = "__LOOP__" + massFitName + "Fit" + binTag(1) + "_bin0_" + RunTag(Run)+ sidebandTag
    figsPerSlide=4
    figsPerRow=4
    
    make_figure_slides(BKG_FIG_FOLDER,path,
                       var_list,"BKG mass fit"+sidebandTitle,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"BkgFit_Jpsi_"+RunTag(Run)+sidebandTag+".tex")
    return 

def genLvlMC(nBins, bin, Run, fold):
    genLvlName = "_genLvl_MC" + rareJpsiTag(False) + "Fit"
    path = "__LOOP__" + genLvlName +  binTagFull(nBins,bin)+ "_OnlyAngles_AllPDFs"
    figsPerSlide=6
    figsPerRow=3

    title = "GenLvl MC fit, "+binTitle(nBins,bin)
    make_figure_slides(GENLVL_FIG_FOLDER,path,
                       angles, title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"GenLvlMC"+binTagFull(nBins,bin)+ foldTag(fold)+"_"+RunTag(Run)+".tex")

    return

def compareGenLvlAndMC(toDavid):
    plotName = "GenLvl_vs_MC_"
    path = plotName + "__LOOP__" + ("_MyVsDavids" if toDavid else "")
    figsPerSlide=8
    figsPerRow=4

    title = "GenLvl vs "
    if (toDavid): title += "David's GenLvl"
    else:         title += "signal MC"

    make_figure_slides(MC_FIG_FOLDER,path,
                       ang_params, title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"GenLvl" + ("_Vs_Davids" if toDavid else "_Vs_SigMC")+".tex")
    return

def compareFolding():
    path = "Folding___LOOP__" 
    figsPerSlide=8
    figsPerRow=4

    title = "Folding comparisons"

    make_figure_slides(MC_FIG_FOLDER,path,
                       ang_params, title,
                       figsPerSlide, figsPerRow,
                       ".eps",TEX_FILE_FOLDER+"Folding_comparison.tex")
    return


#######################

totBins = 5

massFitMCRef(1)
massFitMCRef(2)

for b in range(totBins):
    MCfit(totBins,b,12,-1)

for b in range(totBins):
    genLvlMC(totBins,b,12,-1)
for b in range(8):
    genLvlMC(8,b,12,-1)

compareGenLvlAndMC(False)
compareGenLvlAndMC(True)

BkgFitRef(12,0)

massFitRef(1)
massFitRef(2)
for obsv in mass_plots:
    massFit(totBins,obsv,1)
    massFit(totBins,obsv,2)


for b in range(totBins):
    MCfit(totBins,b,12,0)
    MCfit(totBins,b,12,1)
    MCfit(totBins,b,12,2)
    MCfit(totBins,b,12,3)
    MCfit(totBins,b,12,4)
    
compareFolding()

MainFitToyCheck(True)
MainFitRef(12,True,-1)
MainFitRef(12,False,-1)
MainFitRef(12,False,0)
