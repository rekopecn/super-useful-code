

################################################
### Delete Files Older Than x Days on Linux
################################################

find /path/to/files* -mtime +5 -exec rm {} \;


Note that there are spaces between rm, {}, and \;
Explanation
    The first argument is the path to the files. This can be a path, a directory, or a wildcard as in the example above. I would recommend using the full path, and make sure that you run the command without the exec rm to make sure you are getting the right results.
    The second argument, -mtime, is used to specify the number of days old that the file is. If you enter +5, it will find files older than 5 days.
    The third argument, -exec, allows you to pass in a command such as rm. The {} \; at the end is required to end the command.




################################################
### Searching for strings in files
################################################

grep -rnw '/path/to/somewhere/' -e 'pattern'


    -r or -R is recursive,
    -n is line number, and
    -w stands for match the whole word.
    -l (lower-case L) can be added to just give the file name of matching files.

Along with these, --exclude, --include, --exclude-dir flags could be used for efficient searching:
    This will only search through those files which have .c or .h extensions:

    grep --include=\*.{c,h} -rnw '/path/to/somewhere/' -e "pattern"

    This will exclude searching all the files ending with .o extension:

    grep --exclude=*.o -rnw '/path/to/somewhere/' -e "pattern"

    For directories it's possible to exclude a particular directory(ies) through --exclude-dir parameter. For example, this will exclude the dirs dir1/, dir2/ and all of them matching *.dst/:

    grep --exclude-dir={dir1,dir2,*.dst} -rnw '/path/to/somewhere/' -e "pattern"




################################################
### Rename files
################################################

rename (option) 's/oldname/newname' file1.ext file24.ext  
	-v (verbose: prints the list of renamed files along with their new names)
	-n (“no action:” a test mode or simulation which only shows the files that will be changed without touching them)
	-f (a forced overwrite of the original files)

rename 's/.jpeg/.jpg/' *
