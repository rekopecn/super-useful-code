#!/bin/bash

E_NO_ARGS=65
if [ $# -eq 2 ]  # Must have two command-line args
then
   	job=$1	
	subjob=$2
else
  	echo "Please invoke this script with two command-line arguments in the following format:"
	echo "./open_failed_job_output jobNo subjobs No"
	exit $E_NO_ARGS
fi

#navigate to the correct folder
cd /home/renata/lxplus/afs_user/ 
cd ./gangadir/workspace/rekopecn/LocalXML/$1/$2/output/
gedit Script1_Ganga_GaudiExec.log&	
exit


