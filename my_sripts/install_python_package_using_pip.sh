#!/bin/bash
E_NO_ARGS=65
if [ $# -eq 2 ]  # Must have two command-line args to demo script.
then
   	version=$1
	package=$2
	echo "Installing package $package for python $version"
	read -p "Press enter to continue"
else
  	echo "Please invoke this script with two command-line arguments in the following format:"
	echo "./install_python_package_using_pip.sh VERSION PACKAGE"
	echo "For example: ./install_python_package_using_pip.sh 2.7 numpy"
	exit
fi


wget https://bootstrap.pypa.io/get-pip.py

python$version get-pip.py --user
python -m pip install --user $package 
