import sys
import re
#First argument is the filename
fileName = sys.argv[1]


def stopLoopLinesClass(line):
    if ("public" in line):  return True
    if ("private" in line): return True
    if ("(" in line):       return True
    if (")" in line):       return True
    if ("}" in line):       return True
    return False

def FormatClasses(class_full):        
    class_full = re.split("\n",class_full)

    name = class_full[0].replace("class ","")[0:-1]
    public_vars = []
    private_vars = []
    functions = []

    for idx,line in enumerate(class_full[1:]):
        line = line[4:] #Remove the indent
        if (line.startswith("    ")): continue     
        if (line.startswith("public:") or line.startswith("ic:")):
            idx = idx+1 
            while (not stopLoopLinesClass(class_full[idx+1])):                                
                public_vars.append(class_full[idx+1].lstrip())
                idx = idx+1        
        if (line.startswith("private:") or line.startswith("te:")):
            idx = idx+1 
            while (not stopLoopLinesClass(class_full[idx+1])):
                private_vars.append(class_full[idx].lstrip())
                idx = idx+1       
        if ("(" in line): 
            tmp = line
            if (")" not in line):       
                while (")" not in class_full[idx]):
                    tmp = tmp + class_full[idx].strip()
                    idx = idx+1
                tmp = tmp + class_full[idx].strip()
            functions.append(tmp)         

    return name, public_vars, private_vars, functions

with open(fileName) as f:
    lines = f.readlines()

globals_list = []
function_list = []
classes_list = []


for idx,line in enumerate(lines):
    if (line.startswith("    ")): continue
    if (line.startswith("}")): continue
    if (line.startswith("//")): continue
    if (line.startswith("#")): continue
    if (line.startswith("using")): continue
    if (line.startswith("class")):         
        tmp = ""
        while ("};" not in lines[idx]):
            tmp = tmp + lines[idx]
            idx = idx+1
        tmp = tmp + lines[idx].strip()
        classes_list.append(tmp)
    if (line.isspace()): continue
    if ("(" not in line): globals_list.append(line.strip())
    elif (")" not in line):        
        tmp = ""
        while (")" not in lines[idx]):
            tmp = tmp + lines[idx].strip()
            idx = idx+1
        tmp = tmp + lines[idx].strip()
        function_list.append(tmp)
    else: function_list.append(line.strip())    

for cl in classes_list: FormatClasses(cl)

globals = []
for glob in globals_list:
    glob = glob.replace(";","")
    for tmp in glob.split(','): globals.append(tmp.lstrip())

print("\n\n## Global variables:\n")
for tmp in globals: print("*"+tmp)

print("\n\n# Classes")

for cl in classes_list: 
    name, pub, priv, funcs = FormatClasses(cl)
    print ("### "+name)
    print ("* **Private members:**")
    for p in priv:
        print("    * "+ p.replace(";",""))
    print ("* **Public  members:**")
    for p in pub:
        print("    * "+ p.replace(";",""))
    print ("* **Functions:**")
    for f in funcs:
        f = f.replace("){","")
        f = f.replace(") {","")
        f = f.replace("*","\*")
        funcs = f.split("(")
        funcs[1] = funcs[1].split(',')
        if (len(funcs[1])>0) and (funcs[1][0].strip()!=""): 
            print("    * **"+funcs[0]+"()**")
            print("        * **Parameters**")
            for params in funcs[1]:
                print("            * "+params.lstrip())                        
            print("        * **Return**")
        elif ("~" in funcs[0]): print("    * **"+funcs[0]+"()** // destructor")
        else: print("    * **"+funcs[0]+"()** // constructor")

print("\n\n\n# Functions and their parameters:")
functions = []
for funcs in function_list:
    funcs = funcs.replace("){","")
    funcs = funcs.replace(") {","")
    funcs = funcs.replace("*","\*")
    functions = funcs.split("(")
    functions[1] = functions[1].split(',')
    print("### "+functions[0]+"()\n")
    print("* **Parameters**")
    for params in functions[1]:
        print("    * "+params.lstrip())
    print("* **Return**\n")



