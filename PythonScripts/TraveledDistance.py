import numpy as np

KshortDecayTime = 9*e-11
KlongDecayTime = 5*1e-8

c_m_per_s = 3e8

speed = lambda g: np.sqrt(1.0-(1.0/(g*g)))

distance = lambda g, decayTime: speed(g)*c_m_per_s*g*decayTime

distance_Klong = lambda s: distance (g,KlongDecayTime)
distance_Kshort = lambda s: distance (g,KshortDecayTime)
