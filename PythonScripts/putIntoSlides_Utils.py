from __future__ import print_function
import numpy as np

def RunTag(Run):
    return "Run"+str(Run)

def RunTagEff(Run,RunEff):
    if (Run == RunEff):  
        if (Run == "1"): return "_OnlyRun1"
        if (Run == "2"): return "_OnlyRun2"
        else:          return "_" + RunTag(Run) + "_AllPDFs"    
    else:              return "_Run"+(RunEff)

def RunList():
    return ["1","2","12"]
def RunDict():
    return{ "1" : ["1"],
            "2" : ["2"],
            "12": ["1","2"]
    }
