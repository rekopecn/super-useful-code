import matplotlib as lib
import matplotlib.pyplot as plt
import numpy as np
from textwrap import wrap
from Utils import *

debug = False


#============================
#
#   General options
#
#============================

filePath = "./"
fileName = "mental_health_01.csv"
fileNameOpenQuestions = "openQuestions.txt"

#If no groups are present, set to ["all"]!!!
groups = ["Master student", "PhD", "PostDoc", "Permanent"]
groups = ["all"] 
#groups = ["1 Agree", "2", "3", "4 Disagree"]
group_identifier = 0 #Which question is the identifier? Starting from 0!

# Plotting options
mainColor = ["#1f99ff",'#8931ef', '#f2ca19', '#ff00bd'] #Has to have same lenght as groups!
if (groups == ["all"]): mainColor = ["#1f99ff"] #Set one defualt color in the case of no groups

pieColors = ['#8931ef', '#f2ca19', '#ff00bd', '#0057e9', '#87e911', '#e11845'] #TODO

#============================
#
#   Define all possible lists of answers
#
#============================

#This is a bit annoying in the case of google docs, as a lot of answers just reply with 1 -5 and one has to add the labels. 
# Luckily, in the case of drupal, all have the same values, so just set the labels in a list
lables_dict = {
    "AD4" :     ["","Agree (1)", "(2)", "(3)", "Disagree (4)"],
    "AD5" :     ["","Agree (1)", "(2)", "(3)", "(4)", "Disagree (5)"],
    "GoodBad":  ["","Very good (1)", "(2)", "(3)","(4)","Very bad (5)"],
    "NV":       ["","Not at all", "Not much", "A bit", "Very"],
    "ND":       ["","Not at all", "Several\ndays", "More than\n1/2 the days", "(Nearly)\n every day"],
    }
entries_dict = {
    "AD4" :     {"":0, "1 (agree)":1,     "2":2, "3":3, "4 (disagree)":4},
    "AD5" :     {"":0, "1 (agree)":1,     "2":2, "3":3, "4":4, "5 (disagree)":5},
    "GoodBad":  {"":0, "1 (very good)":1, "2":2, "3":3, "4":4, "5 (very bad)":5},
    "NV":       {"":0, "not at all":1,    "not much":2, "a bit":3, "very":4},
    "ND":       {"":0, "not at all":1, "several days":2, "more than half the days":3, "(nearly) every day":4},
    }

def questionSettings():
    #Set which chart is a pie chart or bar chart by hand
    #In case of a bar chart, sets labels and parse answers
    return [ "pie",  #Who are you
            "pie", "pie", "bar_AD4", "bar_AD4", "bar_AD4", "bar_AD4", "bar_AD4", "bar_AD4","bar_AD4", #9
            "bar_AD4","pie", "bar_GoodBad", #Home Office
            "bar_GoodBad","bar_GoodBad","bar_GoodBad","pie","pie","pie","pie", #19
            "bar_NV", "bar_NV", "bar_NV", "bar_NV", "bar_NV", "bar_NV", "bar_NV", "bar_ND",  "bar_ND",  "bar_ND",  #29
            "bar_ND", "bar_ND", "bar_ND", "bar_ND", "bar_ND", "bar_ND", "bar_ND", "bar_ND", "pie",  "bar_GoodBad", #39 #Mental health
            "bar_AD5", "bar_AD5", "bar_AD5", "bar_AD5", "bar_AD5", "pie", "pie", "open", #Mood meter
            "open","open" #49
    ]

#============================
#
#   Remove useless answers/questions
#
#============================

def strip_arr_from_useles_data(arr_answers): #Modify this accordingly to your questionnaire!
    arr_answers = arr_answers[20:]
    return arr_answers

def get_arr_from_CSV_stripped():
    return strip_arr_from_useles_data(get_arr_from_CSV(filePath,fileName))

#Return a dictionary of answers, if there are no groups, default key is "all"
def get_answers_dict():
    if (groups == ["all"]): return {"all": get_arr_from_CSV_stripped()}
    arr_answers = get_arr_from_CSV_stripped().T
    
    #Create an empty dictionary with groups as keys
    dict_answers =  {key: [] for key in groups}
    dict_answers_tmp = {key: [] for key in groups}

    arr_questions = arr_answers[0] #Keep the questions separate

    for answer in arr_answers[1:]: #zeroth one is the questions
        #Sort the answers into groups, the item is a list of arrays
        if (answer[group_identifier] in groups): dict_answers_tmp.get(answer[group_identifier]).append(answer)

    for key in dict_answers.keys(): #Add the questions back to each group on the first position
        if len(dict_answers_tmp[key]) == 0 : continue #Protect empty answers 
        #Connect the list of array with the questions
        dict_answers[key] = np.vstack((arr_questions,dict_answers_tmp[key])).T
    return dict_answers    

#============================
#
#       Bar plotting
#
#============================

#Returns int values for each answer and corresponding labels  
#This is needed for bar charts, as some values might be empty and skewing the whole plotting of histograms
def parse_answers(barType, entry):
    tmp_list = []
    dict_tmp = entries_dict[barType]
    labels_list = lables_dict[barType]
    
    for ent in entry:
        if (ent.lower() in dict_tmp): tmp_list.append(dict_tmp.get(ent.lower()))
    #returns an array of answers, an array with labels and an array with values
    #Technically not needed for each group, but in case of one group this is easier
    return np.array(tmp_list), labels_list, list(dict_tmp.values())   

# Create a bar chart from a dictionary of entries, tag setting the bar type and the name of the outputfile
def make_bar(entry_dict, settings, filename):


    #Creat a figure
    fig  = plt.figure()  
    title  = entry_dict[groups[0]][0] #Take the title from the first group    
    fig.text(0.5, 0.95, title, size='large', weight='bold',  
            horizontalalignment='center', verticalalignment='center', wrap=True) 
    
    answer_list = [] #All answers need to be put in a list in order to the histtype='bar' to work
                     #Yes, there is probably an easier way, but this works nicely  

    #Loop over the entry dictionary        
    for num,entry in enumerate(entry_dict.values(),start = 0):
        answers, labels, values = parse_answers(strip_bar(settings),entry[1:])  
        answer_list.append(answers)
        #answer_list.append(np.delete(answers,np.argwhere(answers==0)))#Remove empty answers
        
    #Plot the histograms as a bar chart
    bin_edges = [x+0.5 for x in values]
    bar_chart = plt.hist(answer_list, bins=bin_edges, range = (bin_edges[0],bin_edges[-1]), histtype='bar', density=False, rwidth= 1.0/(len(groups)+1.0), color = mainColor, label = groups)    

    #Set the xlabels
    plt.xticks(values, labels)

    #Plot the legend
    plt.legend(loc='upper left', prop={'size': 13})
    # Save and close
    fig.savefig(filename)
    plt.close()

# Create an open bar chart per group #TODO: should be grouped
def make_openBar(entry, settings, filename):

    title  = entry[0]
    total  = len(entry)-1
    answers = []
    for ans in entry[1:]: #Split all the answers 
        answers = answers + ans.split(';')     

    #Get counts of all answers
    answers, sizes = np.unique(answers, return_counts = True) 

    if (debug): 
        print ("Title:",title)
        print ("Total:",total)
        print (answers)

    # make the bar chart
    fig  = plt.figure()  
    fig.text(0.5, 0.95, title, size='large', weight='bold', 
            horizontalalignment='center', verticalalignment='center', wrap=True)
    
    #Set the legend        
    fig.subplots_adjust(left=0.1, right=0.99, top=0.9, bottom=0.2)

    #Plot the bar chart
    bar_chart = plt.bar(answers, sizes) 

    #Make pretty x-axis labels
    rotation='vertical'
    labels = [ "\n".join(wrap(x,10)) for x in answers ] #wrap the answers
    plt.xticks(answers, labels, rotation=0)
    
    # Save and close
    fig.savefig(filename)
    plt.close()

#============================
#
#       Pie plotting
#
#============================
    
# Create a pie chart
def make_pie(entry,settings,filename):

    # Preprocess the entry
    title  = entry[0]
    
    answers = np.delete(entry[1:],np.argwhere(entry[1:]=='')) #Remove empty answers
    total  = answers.size  

    answers, sizes = np.unique(answers, return_counts = True)               
    #if (debug): print (title, answers, sizes)

    # Set the pyplot figure
    fig = plt.figure()
    fig.text(0.5, 0.94, title, size='large', weight='bold', wrap = True,
            horizontalalignment='center', verticalalignment='center')
    fig.subplots_adjust(left=0.01, right=0.9, top=0.85, bottom=0.01)

    # make the pie chart
    myplot = fig.add_subplot(1, 1, 1)
    myplot.axis('equal')
    box = myplot.get_position()
    myplot.set_position([box.x0, box.y0, box.width * 0.65, box.height])

    patches = myplot.pie([float(x)/float(total) for x in sizes], colors=pieColors, 
                         autopct='%1.1f%%', shadow=False, startangle=90)


    answers = [ "\n".join(wrap(x,18)) for x in answers ] #wrap the answers
    plt.legend(patches[0], answers, loc='center left', bbox_to_anchor=(1.0, 0.65))

    # Save and close
    fig.savefig(filename)
    plt.close()

#============================
#
#       Open questions
#
#============================

# Save answers to open questions in a separate file
def save_open_answer(entry, settings, filename):

    #Open txt file for open questions    
    fileOpenAnswers = open(filename+".txt",'w')
    title  = entry[0]
    answers = entry[1:]
    answers = np.delete(answers,np.argwhere(answers=='')) #Remove empty answers
    #Print all answers
    print(title, answers, "\n", file = fileOpenAnswers)
    #Close
    fileOpenAnswers.close()   


#============================
#
#       Correlations
#
#============================

# Save answers to open questions in a separate file
def save_correlation(entry_1, entry_2, settings_1,settings_2, filename):
    
    settings_1 = strip_bar(settings_1) #Strip the bar from the options
    settings_2 = strip_bar(settings_2) 

    #Strip the question from the answers values and turn the answers into values
    #Yes, it could've been done by a list of len()=2, but this seems easier to read
    answers_1, labels_1, values_1 = parse_answers(strip_bar(strip_bar(settings_1)),entry_1[1:])
    answers_2, labels_2, values_2 = parse_answers(strip_bar(strip_bar(settings_2)),entry_2[1:])

    #Remove the pairs with no answer
    empty = np.append(np.argwhere(answers_1==0),np.argwhere(answers_2==0))
    answers_1 = np.delete(answers_1,empty) 
    answers_2 = np.delete(answers_2,empty) 

    corr_matrix = np.corrcoef(answers_1,answers_2)
    if (abs(corr_matrix[0,1]<0.0)): return
   
    #Plot the histograms as a 2D hist
    lib.rcParams['figure.figsize'] = [9, 8] # for squareish canvas
    fig  = plt.figure()  
    title  = "Correlation = " + str(round(corr_matrix[1,0],2))
    fig.text(0.5, 0.95, title, size='large', weight='bold',  
            horizontalalignment='center', verticalalignment='center', wrap=True, label = "") 

    bin_edges_1 = [x+0.5 for x in values_1]
    bin_edges_2 = [y+0.5 for y in values_2]
    print (bin_edges_1, bin_edges_2)
   
    #screw plt.hist2d, let's do numpy
    hist, bin_edges_1, bin_edges_2 = np.histogram2d(answers_1,answers_2, bins = (bin_edges_1,bin_edges_2))
    
    #Well this is stupid, but whatever
    corr_plot = plt.imshow(hist, interpolation='nearest', origin='low',  extent=[bin_edges_1[0]-0.5, bin_edges_1[-1], bin_edges_2[0]-0.5, bin_edges_2[-1]])

    
    #Add the color bar
    plt.colorbar()
    #Set the xlabels
    plt.xticks(values_1, labels_1)
    plt.yticks(values_2, labels_2, rotation=90)
    plt.xlabel(entry_1[0] )#Take the title from the first element
    plt.ylabel(entry_2[0] )#Take the title from the first element

    # Save and close
    fig.savefig(filename, bbox_inches='tight')
    plt.close()


#============================
#
#       Time plot
#
#============================

def make_time(entry, settings, filename):
    title  = entry[0]
    total  = len(entry)-1
    answers= entry[1:]
    answers_in_seconds = []

    #Get timestamp to seconds 
    from datetime import datetime
    import time
    for answ in answers:
        
        answers_in_seconds.append(time.mktime(get_time(answ).timetuple()))        

    #set start to the first entry and set the seconds to hours (days)
    arr_answers = (np.array(answers_in_seconds) -   answers_in_seconds[0] ) / (3600.0*24)
    #Get binning
    step = 1 #(arr_answers[-1]-arr_answers[0])/20
    arr = np.arange(arr_answers[0]-step/2, arr_answers[-1]+step,step)
   
    if (debug): 
        print ("Title:",title)
        print ("Total:",total)
        print (arr_answers)        
        print (arr)

    # make the histogram
    fig  = plt.figure()  
    fig.text(0.5, 0.95, title, size='large', weight='bold', 
            horizontalalignment='center', verticalalignment='center', wrap=True)

    #Plot the histogram
    bar_chart = plt.hist( arr_answers,bins = arr)
    plt.xlabel("Days from the email") #technically from the first entry

    # Save and close
    fig.savefig(filename)
    plt.close()

#============================
#
#       Plot everything
#
#============================

def make_plots():

    setLib() #Set plotting (see Utils.py)
    
    #Read the csv file
    main_data = get_answers_dict()
    
    '''
    #Plot pie charts first, as they are plot separately for each group
    for group, answers in main_data.items(): 
        if (len(answers) == 0 ): continue #Protection from empty groups
        for nPlot, entry in enumerate(answers,start=0): #Loop over answers in each group
            settings = questionSettings()[nPlot]             
            if ("pie" in settings): make_pie(entry,settings,"plots/pie_"+str(nPlot)+"_"+group+".png")
            elif ("open" in settings): save_open_answer(entry, settings, "open_"+str(nPlot)+"_"+group+".txt")
            print("Processed pie and open question ", nPlot)
    
    #Make bar plots with groups next to each other    
    #Create a dictionary for single answer
    
    entry_dict =  {key: [] for key in groups}
    for nPlot in range(0,main_data[groups[0]].T[0].size-1): #CAREFUL! Here I assume the number of questions is the same for everyone! It will also fail if a group is not represented in the answers!    
        settings = questionSettings()[nPlot]
        if ("bar" not in settings): continue
        
        #group dictionary for single answer
        for group in groups: entry_dict[group] = main_data[group][nPlot]        
        if (debug): print (entry_dict)

        group_name = "all" if (groups == ["all"]) else "_grouped" 
        make_bar(entry_dict,settings,"plots/bar_"+str(nPlot)+"_"+group_name+".png")
        entry_dict =  {key: [] for key in groups} #Clean the dictionary in case there will be empty answers

        print("Processed bar question ", nPlot)
    '''
    
    #Plot correlations, plot separately for each group
    for group, answers in main_data.items(): 
        if (len(answers) == 0 ): continue #Protection from empty groups
        for nPlot_1, entry_1 in enumerate(answers,start=0): #Loop over answers in each group
            settings_1 = questionSettings()[nPlot_1]    
            if ("bar" not in settings_1): continue
            for nPlot_2, entry_2 in enumerate(answers[nPlot_1+1:],start=nPlot_1+1): #Loop over answers in each group                
                settings_2 = questionSettings()[nPlot_2]    
                if ("bar" not in settings_2): continue
                print("Processing question ", nPlot_1, "vs", nPlot_2)
                save_correlation(entry_1,entry_2,settings_1,settings_2,"plots/corr/corr_"+str(nPlot_1)+"vs" +str(nPlot_2) + "_" +group+".png") 


make_plots()