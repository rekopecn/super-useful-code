

from textwrap import wrap
import matplotlib as lib
import matplotlib.pyplot as plt

debug = False

SR = {
    '1' :1,
    '2' :2,
    '3' :3,
    '4' :4,
    '5' :5} #Standard range
AD4= { #Agree-disagree, 4 levels
    "Agree (1)" : 1,
    "(2)": 2,
    "(3)": 3,
    "Disagree (4)" : 4}
AD5 ={ #Agree-disagree, 5 levels
    "Agree (1)" : 1,
    "(2)": 2,
    "(3)": 3,
    "(4)": 4,
    "Disagree (5)" : 5}
NV = { #Not - very
    "Not at all" : 1,
    "Not much" : 2,
    "A bit": 3,
    "Very" : 4
    } 
ND = { #Not - daily
    "Not at all" : 1, 
    "Several days" : 2, 
    "More than half the days": 3, 
    "(Nearly) every day": 4}

special_labels = {
    "_basic" : ["Not at all (1)", "(2)", "(3)","(4)","Very (5)"],
    "_TooLittleTooMuch" : ["Too little (1)", "(2)", "(3)","(4)","Too much (5)"],
    "_NeverAlways" : ["Never (1)", "(2)", "(3)","(4)","Always (5)"],
    "_BadGreat" : ["Bad (1)", "(2)", "(3)","(4)","Great (5)"],
    "_BasicAdvanced" : ["Too basic (1)", "(2)", "(3)","(4)","Too advanced (5)"],
    "_GoodBad" : ["Very good (1)", "(2)", "(3)","(4)","Very bad(5)"],
    "ND": ["Not at all", "Several\ndays", "More than\n1/2 the days", "(Nearly)\n every day"]}


mainColor = "#1f99ff"

pieColors = ['#8931ef', '#f2ca19', '#ff00bd', '#0057e9', '#87e911', '#e11845'] #TODO

def setLib():
    # Library settings
    lib.rc('patch', edgecolor='white')
    lib.rc('font', family='DejaVu Sans', size=13)
    lib.rc('legend', fontsize=13, frameon=0)
    lib.rc('figure.subplot', wspace=0, hspace=0)

def whichPlot(whatType, plot_pie, plot_openbar, plot_bar, save_open, plot_time):
    setLib() # Library settings
    #Decide if to plot pie, open or bar chart
    #In the case of bar chart, create new labels to answers    
    if (whatType == "pie" and plot_pie): return True 
    if (whatType == "b_op" and plot_openbar): return True
    if (whatType == "open" and save_open): return True
    if ("bar" in whatType and plot_bar): return True
    if ("time" in whatType and plot_time): return True
    else: return False       

def parse_answers(whatType, entry):
    tmp_list = []
    dict_tmp = {}
    keys_list = []
    if    ("SR" in whatType): dict_tmp = SR  
    elif ("AD4" in whatType): dict_tmp = AD4
    elif ("AD5" in whatType): dict_tmp = AD5
    elif ("NV" in whatType):  dict_tmp = NV
    elif ("ND" in whatType):  dict_tmp = ND

    for ent in entry:
        if (ent in dict_tmp): tmp_list.append(dict_tmp.get(ent))
    
    keys_list = list(dict_tmp.keys())
    if ("SR" in whatType): keys_list = special_labels.get(whatType.replace("barSR",""))
    if ("ND" in whatType): keys_list = special_labels.get("ND")
    return np.array(tmp_list), [""]+keys_list, list(dict_tmp.values())   
    
def questionSettings():
    #Set which chart is a pie chart or bar chart by hand
    #In case of a bar chart, set labels and parse answers

    return [ "time", #Timestamp
            "barSR_basic", "barSR_basic", "barSR_basic", "barSR_basic", "barSR_TooLittleTooMuch", "open", #General RTG questions
             "barSR_NeverAlways", "barSR_BadGreat", "barSR_basic", "pie", "pie", "barSR_basic", "open", #Student lectures
             "pie", "open", "open", #Colliding pizza
             "pie", #Physics teams
             "pie", "pie", "b_op", "open", #Retreat
             "pie","pie","b_op","open", #Coaching
             "pie", "barSR_basic", "barSR_basic", "pie", #Gender initiative
            "pie", "barSR_GoodBad", "barSR_basic", "pie", "barSR_GoodBad", "barSR_basic", "barSR_basic", "barSR_BasicAdvanced", "open", #Colloqioum
            "pie","pie","barAD4","barAD4","barAD4","barAD4","barAD4","barAD4","barAD4","barAD4","pie", #Home office
            "barSR_GoodBad", "barSR_GoodBad", "barSR_GoodBad", "pie", "pie","barNV","barNV","barNV","barNV","barNV","barNV","barNV",
            "barND","barND","barND","barND","barND","barND","barND","barND","barND","barND","barND","b_op", #Mental health
            "barAD5", "barAD5", "barAD5", "barAD5", "barAD5","pie", #Mood meter
            "open","open"
    ]

# Create a pie chart
def make_pie(entry,settings,filename):

    # Check if it's a pie chart
    if not (whichPlot(settings,True,False,False,False,False)): return

    # Predzpracovani vstupu
    title  = entry[0]
    answers = np.delete(entry[1:],np.argwhere(entry[1:]=='')) #Remove empty answers
    total  = answers.size  

    answers, sizes = np.unique(answers, return_counts = True)           

    if (debug): print (answers, sizes)

    # Set the pyplot figure
    fig = plt.figure()
    fig.text(0.5, 0.94, title, size='large', weight='bold', wrap = True,
            horizontalalignment='center', verticalalignment='center')
    fig.subplots_adjust(left=0.01, right=0.9, top=0.85, bottom=0.01)

    # make the pie chart
    myplot = fig.add_subplot(1, 1, 1)
    myplot.axis('equal')
    box = myplot.get_position()
    myplot.set_position([box.x0, box.y0, box.width * 0.65, box.height])

    patches = myplot.pie([float(x)/float(total) for x in sizes], colors=pieColors, 
                         autopct='%1.1f%%', shadow=False, startangle=90)


    answers = [ "\n".join(wrap(x,18)) for x in answers ] #wrap the answers
    plt.legend(patches[0], answers, loc='center left', bbox_to_anchor=(1.0, 0.65))

    # Save and close
    fig.savefig(filename)
    plt.close()

# Create a bar chart
def make_bar(entry, settings, filename):

    # Check if it's a bar chart
    if not (whichPlot(settings,False,False,True,False,False)): return

    title  = entry[0]
    total  = len(entry)-1
    answers, labels, values = parse_answers(settings,entry[1:])

    if (debug): 
        print ("Title:",title)
        print ("Total:",total)
        print (answers,labels)        

    # make the bar chart
    fig  = plt.figure()  
    fig.text(0.5, 0.95, title, size='large', weight='bold', 
            horizontalalignment='center', verticalalignment='center', wrap=True)

    values.insert(0,0) #Add a zero to the begining, as we need nBins+1 edges    
    bar_chart = plt.hist(answers,bins=[x+0.5 for x in values], density=False, width=0.8, color = mainColor) 

    #Set the xlabels
    plt.xticks(values, labels)

    # Save and close
    fig.savefig(filename)
    plt.close()

# Create an open bar chart
def make_openBar(entry, settings, filename):

    # Check if it's a bar chart
    if not whichPlot(settings,False,True,False,False,False): return

    title  = entry[0]
    total  = len(entry)-1
    answers = []
    for ans in entry[1:]:
        answers = answers + ans.split(';')     

    answers, sizes = np.unique(answers, return_counts = True)

    if (debug): 
        print ("Title:",title)
        print ("Total:",total)
        print (answers)

    # make the pie chart
    fig  = plt.figure()  
    fig.text(0.5, 0.95, title, size='large', weight='bold', 
            horizontalalignment='center', verticalalignment='center', wrap=True)
    fig.subplots_adjust(left=0.1, right=0.99, top=0.9, bottom=0.2)

    bar_chart = plt.bar(answers, sizes) 
    rotation='vertical'
    labels = [ "\n".join(wrap(x,10)) for x in answers ] #wrap the answers
    plt.xticks(answers, labels, rotation=0)
    
    # Save and close
    fig.savefig(filename)
    plt.close()

# Save answers to an open question
def save_open(entry, settings, f):

    # Check if it's an open question
    if not whichPlot(settings, False, False, False, True, False): return

    title  = entry[0]
    answers = entry[1:]
    answers = np.delete(answers,np.argwhere(answers=='')) #Remove empty answers
    print(title, answers, "\n", file = f)

# Create a bar chart
def make_time(entry, settings, filename):

    # Check if it's a bar chart
    if not (whichPlot(settings,False,False,False,False,True)): return

    title  = entry[0]
    total  = len(entry)-1
    answers= entry[1:]
    answers_in_seconds = []

    #Get timestamp to seconds 
    from datetime import datetime
    import time
    for answ in answers:
        answ = answ.replace(" CET","")
        answ = answ.replace(" am","")
        answ = answ.replace(" pm","")
        d = datetime.strptime(answ, "%Y/%m/%d %H:%M:%S")
        print (d)

        answers_in_seconds.append(time.mktime(d.timetuple()))        

    #set start to the first entry and set the seconds to hours (days)
    arr_answers = (np.array(answers_in_seconds) -   answers_in_seconds[0] ) / (3600.0*24)
    #Get binning
    step = 1 #(arr_answers[-1]-arr_answers[0])/20
    arr = np.arange(arr_answers[0]-step/2, arr_answers[-1]+step,step)
   
    if (debug): 
        print ("Title:",title)
        print ("Total:",total)
        print (arr_answers)        
        print (arr)

    # make the histogram
    fig  = plt.figure()  
    fig.text(0.5, 0.95, title, size='large', weight='bold', 
            horizontalalignment='center', verticalalignment='center', wrap=True)

    bar_chart = plt.hist( arr_answers,bins = arr)
    plt.xlabel("Days from the email") #technically from the first entry

    # Save and close
    fig.savefig(filename)
    plt.close()


import csv 
import numpy as np

def readCSV(isCoaching, isNoCoaching):
    fileName = "RTG_feedback_ 2020.csv"
    folderName = "All"
    if (isCoaching): 
        fileName = "RTG_feedback_ 2020_Coaching.csv"
        folderName = "Coaching"
    if (isNoCoaching):
        fileName = "RTG_feedback_ 2020 _noCoaching.csv"
        folderName = "NoCoaching"

    fileOpenAnswers = open(folderName+"_openQuestions.txt",'w')
    with open(fileName) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        i = 0
        tmp_list = []
        for row in csvreader:
            tmp_list.append(row)

        arr_answers = np.array(tmp_list)    
        nFile = 0

        for entry in arr_answers.T:       
            settings = questionSettings()[nFile] 
            make_bar(entry,     settings, "plots/"+folderName+"/bar_"+str(nFile)+".png") 
            make_pie(entry,     settings, "plots/"+folderName+"/pie_"+str(nFile)+".png") 
            make_openBar(entry, settings, "plots/"+folderName+"/openBar_"+str(nFile)+".png") 
            save_open(entry,    settings, fileOpenAnswers)
            make_time(entry,    settings, "plots/"+folderName+"/time_"+str(nFile)+".png") 
            print("Processed answer ", nFile)
            nFile +=1
    fileOpenAnswers.close()   

readCSV(False,False)
#readCSV(True,False)
#readCSV(False,True)