#########################################################
#							                            #
#       	Script for easier pushing to git    	    #
#                                                       #
#########################################################

#Bash would work but this is nicer for the user to read

import git 
import sys
import subprocess

if (len(sys.argv)!= 3): 
    print("Use it only with two arguments! python git.py fileToBePushed commitMessage")
    print("Using " + str(len(sys.argv)) + " arguments")
    exit()

toBePushed = sys.argv[1]
commitMessage = sys.argv[2]

class_dict = {
    "generator":        "sources/Core/bu2kstarmumu_generator",
    "loader":           "sources/Core/bu2kstarmumu_loader",
    "parameters":       "sources/Core/bu2kstarmumu_parameters",
    "pdf":              "sources/Core/bu2kstarmumu_pdf",
    "plotter":          "sources/Core/bu2kstarmumu_plotter",
    "event":            "sources/Core/event",
    "fitter":           "sources/Core/fitter",
    "funcs":            "sources/Core/funcs",
    "options":          "sources/Core/options",
    "design":           "sources/Helpers/design",
    "helpers":          "sources/Helpers/helpers",
    "constants":        "sources/Params/constants",
    "angularcorr":      "sources/Run/angularcorr",
    "backgroundfit":    "sources/Run/backgroundfit",
    "feldman_cousins":  "sources/Run/feldman_cousins",
    "genlvlfit":        "sources/Run/genlvlfit",
    "likelihoodscan":   "sources/Run/likelihoodscan",
    "mainfit":          "sources/Run/mainfit",
    "massfit":          "sources/Run/massfit",
    "momfit":           "sources/Run/momfit",
    "multifit":         "sources/Run/multifit",
    "pulls":            "sources/Run/pulls",
    "toys":             "sources/Run/toys",
    "help":             "sources/help",
    "paths":            "sources/paths"
}

others_dict ={ 
    "main":          "bu2kstarmumu.cc",
    "toystudy":      "sources/Core/toystudy.hh",
    "parameters":    "sources/Params/parameters.hh",
    "parameterscan": "sources/Params/parameterscan.hh",
    "values":        "sources/Params/values.hh",
}


mainPath =  "/home/lhcb/kopecna/B2KstarMuMu/code/ewp-Bplus2Kstmumu-AngAna/FCNCfitter/"
if (toBePushed in class_dict): #TODO: it is rahter slow, switch back to popen
    add_cc = subprocess.run(["git","add",class_dict.get(toBePushed)+".cc"], cwd = "./", check = False) #Output into console 
    add_hh = subprocess.run(["git","add",class_dict.get(toBePushed)+".hh"], cwd = "./", check = False) #Output into console
elif (toBePushed in others_dict):  
    add = subprocess.run(["git","add",others_dict.get(toBePushed)], cwd = "./", check = False) #Output into console
else:
    print("Wrong file name! Use any of the following:")    
    print (class_dict.keys())
    print (others_dict.keys())

add = subprocess.run(["git","commit","-m",commitMessage], cwd = "./", check = True) #Output into console
add = subprocess.run(["git","push","origin","renata"], cwd = "./", check = True) #Output into console

print ("\nDone pushing.\n")


