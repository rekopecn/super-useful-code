import datetime
import matplotlib as lib
import csv 
import numpy as np

debug = False
whatTime = "Drupal"

# Get time from Google form 
def get_time_Google(timeStamp):
    timeStamp = timeStamp.replace(" CET","")
    d = datetime.strptime(timeStamp, "%Y/%m/%d %I:%M:%S p")
    if (debug): print (d)
    return d

# Get time from Drupal form 
def get_time_Drupal(timeStamp):
    d = datetime.strptime(timeStamp, "%Y-%m-%d %H:%M:%S")
    if (debug): print (d)
    return d    

#Get whatever time you specify by whatTime
def get_time(timeStamp):
    if (whatTime == "Drupal"): return get_time_Drupal(timeStamp)
    elif (whatTime == "Google"): return get_time_Drupal(timeStamp)
    else: print ("Wrong whatTime! Check Utils.py")
    return

#Set plot options
def setLib():
    # Library settings
    lib.rc('patch', edgecolor='white')
    lib.rc('font', family='DejaVu Sans', size=13)
    lib.rc('legend', fontsize=13, frameon=0)
    lib.rc('figure.subplot', wspace=0, hspace=0)



#Read the lines from CSV files 
def get_arr_from_CSV(filePath, fileName):
    with open(filePath+fileName) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        tmp_list = []        
        for row in csvreader:
            tmp_list.append(row)
        arr_answers = np.array(tmp_list)    
        arr_answers = arr_answers.T           

    return arr_answers

#Strip bar from options for plotting bars
#Bar otpion always has to contain bar_ and the type of answer specifying the entries_dict and labels_dict 

def strip_bar(settings): #I love the name
    return settings.replace("bar_","")