from __future__ import print_function
import numpy as np 

from putIntoSlides_Utils import *


FIG_FOLDER = "/home/renata/B2KMuMu/sigma0/B2KstarMuMu/code/ewp-Bplus2Kstmumu-AngAna/FCNCfitter/plots/MCFit"
decName = "KplusPi0Resolved"
angles = ["ctk", "ctl", "phi"]
params  = ["Fl",
        "S3",
        "S4",
        "S5",
        "Afb",
        "S7",
        "S8",
        "S9",
        "m_b",
        "m_sigma_1",
        "alpha_1",
        "alpha_2",
        "n_1",
        "n_2"
]

def make_figure_slides(folder, mainName, listOfEntries, defaultTitle, figsPerSlide, figsPerRow, figType, outputFile):
    
    #Get width of the pic based on the number of plots per row
    picWidth = 0.95/figsPerRow    
    
    #Pass in the mainName of the figure with __LOOP__ where the loop should happen
    lineList = []
    for num,x in enumerate(listOfEntries,start =1):
        name = mainName.replace("__LOOP__",x,1)
        newLine = "\\\\" if (num%figsPerRow==0) else "" #Put // at the end of each row
        lineList.append("\t\\includegraphics[width=" + "%.2f" %picWidth  +"\\textwidth]{" + folder + name + figType + "}"+newLine)

    #Open the file
    f = open(outputFile,'w')
    for num,line in enumerate(lineList, start = 1):
        if ((num-1)%figsPerSlide==0): #Print the begining of a slide
            print("\\begin{frame}{"+defaultTitle+"}",file = f) #TODO: Replace the list of titles by something sensible
            print("",file = f)
        print(line, file =f) #Print the includegraphics
        if (num%figsPerSlide==0):  #Print the end of a slide
            print("\\end{frame}",file = f)
            print("%-----------------------------------------",file = f)
            print("",file = f)           
    if (len(lineList)%figsPerSlide !=0):
        print("\\end{frame}",file = f)
    f.close()

    print("\\include{"+outputFile+"}")

def ctk_slides(Run):
    listOfEntries = ["55","56","57","58","59","60","61","62","63","64","65"]#list(map(str,np.arange(0,9,1)))
    defaultTitle = "\ctk projection of legendre pol, Run "+ str(Run)
    figsPerSlide=8
    figsPerRow=4
    make_figure_slides(FIG_FOLDER+"test/", "Run"+Run+"/ctkeff_KplusPi0Resolved_Run"+Run+"_2__LOOP__6", listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", "ctkeff_"+Run+".tex")

def projections(angle,Run):
    listOfEntries = list(map(str,np.arange(0,18,1)))
    #listOfEntries.append("") #All q2
    defaultTitle = angle + " projections, " + RunTag(Run)
    outputName = angle +  "proj_" + RunTag(Run)
    figsPerSlide=6
    figsPerRow=3
    make_figure_slides("./figures/final_param/",angle+"eff__LOOP___" + decName + "_" + RunTag(Run), listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", outputName+".tex")

def fits_per_var(Run,var):
    listOfEntries = ["0","1","2","3"]#list(map(str,np.arange(0,9,1)))
    defaultTitle = "var "+var+ ", Run "+ str(Run)
    figsPerSlide=4
    figsPerRow=4
    make_figure_slides(FIG_FOLDER+"fit/", var+"_MC_SignalFit_ALLBINS_bin__LOOP__IndividualFit_OnlyRun"+Run+"_Run"+Run, listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", var+"_"+Run+".tex")

def fits_per_bin(Run, bin):
    listOfEntries = ["mlog","m","ctl","ctk","phi"]#list(map(str,np.arange(0,9,1)))
    defaultTitle = "bin "+str(bin)+ ",Run "+ str(Run)
    figsPerSlide=5
    figsPerRow=3
    make_figure_slides(FIG_FOLDER+"fit/", "__LOOP___MC_SignalFit_ALLBINS_bin"+bin+"IndividualFit_OnlyRun"+Run+"_Run"+Run, listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", "bin" + bin +"_"+Run+".tex")

def fits_per_bin_Jpsi(Run, bin):
    listOfEntries = ["mlog","m","ctl","ctk","phi"]#list(map(str,np.arange(0,9,1)))
    defaultTitle = "bin "+str(bin)+ ",Run "+ str(Run)
    figsPerSlide=5
    figsPerRow=3
    make_figure_slides(FIG_FOLDER+"fit/Jpsi/", "__LOOP___MC_JpsiFit_1BIN_bin"+bin+"IndividualFit_OnlyRun"+Run+"_Run"+Run, listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", "Jpsi_bin" + bin +"_"+Run+".tex")

def fits_per_bin(Run, RunEff, bin, Jpsi, nBins, simFit):
    listOfEntries = ["mlog","m","ctl","ctk","phi"]#list(map(str,np.arange(0,9,1)))
    dataTag = "Jpsi" if Jpsi else "Signal"
    simTag = "_SimultaneousFit" if simFit else "_IndividualFit"
    totbinTag = ""
    if   (nBins==1):  totbinTag = "1BIN"
    elif (nBins==4):  totbinTag = "ALLBINS"
    else:             totbinTag = str(nBins)+"BINS"
    
    defaultTitle = "MC fit "+dataTag+", bin "+str(bin)+ "/"+totbinTag+",Run "+ str(Run)+",Run "+ str(RunEff)
    outputName = dataTag+"/bin"+str(bin)+ "_"+totbinTag + simTag +RunTagEff(Run, RunEff)

    figsPerSlide=5
    figsPerRow=3
    make_figure_slides(FIG_FOLDER+"/"+dataTag+"/", "__LOOP___MC_"+dataTag+"Fit_"+totbinTag+"_bin"+bin+simTag+RunTagEff(Run,RunEff), listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", outputName+".tex")

def parComparison(PHSP):
    listOfEntries = params
    phpsTag = "PHSP" if PHSP else ""
    figsPerRow = 3
    figsPerSlide = 6
    
    defaultTitle = "MC fit "+phpsTag
    outputName =  "compare" + phpsTag

    figsPerSlide=6
    figsPerRow=3
    make_figure_slides(FIG_FOLDER+"/"+phpsTag+"/Signal/", "__LOOP__", listOfEntries, defaultTitle, figsPerSlide, figsPerRow, ".eps", outputName+".tex")


#Just dumps all figures from the file 
def read_ls(path):
    import subprocess
    from subprocess import Popen, PIPE

    command = subprocess.Popen('ls '+path, shell=True, stdout=subprocess.PIPE)	
    stdout = command.communicate()[0] #gives the return of the ls, second member of the list is not important
    return stdout.decode().split('\n') #splits the string into a list of files that are in the folder    

# Put figs in one slide changing with \only
def print_figures_from_ls(path):
    for count, fig in enumerate(read_ls(path),1):
        print("\\only<%i>{\\includegraphics[width=0.99\\textwidth]{%s}}" % (count,fig))
    return    
def print_figures_from_ls_plus(path):
    for fig in read_ls(path):
        print("\\only<+>{\\includegraphics[width=0.99\\textwidth]{%s}\\addtocounter{framenumber}{1}}" % (fig))
    return    



#for run in RunDict().keys():
#    for runEff in RunDict().get(run):
#        for bin in range(0,4):
#            fits_per_bin(run, runEff, str(bin), False, 4, True)
#        #fits_per_bin(run, runEff, str(0), False, 1, False)
#        #fits_per_bin(run, runEff, str(0), True, 1, False)
#        # 
#for bin in range(0,4):
#    fits_per_bin("12", "12", str(bin), False, 4, False)
#    #fits_per_bin("12", "12", str(bin), False, 4, True)
#    #for runEff in RunDict().get("12"):
#        #fits_per_bin("12", runEff, str(bin), False, 4, True)
#        #fits_per_bin("12", runEff, str(bin), False, 4, False)

#
#fits_per_bin("12", "1", str(0), False, 1, True)
#fits_per_bin("12", "2", str(0), True,  1, True)
#fits_per_bin("12", "12", str(0), False, 1, True)
#fits_per_bin("12", "12", str(0), True,  1, True)
#fits_per_bin("12", "12", str(0), True,  1, False)
####
#for ang in angles:
#    projections(ang,"1")
#    projections(ang,"2")
parComparison(True)
parComparison(False)