FileName = "Velo_fit_Data_2015_Fine_MotherCut.out"

print
print
print FileName
print
print
counter = 10;

with open(FileName, 'r') as inF:
    for line in inF:
            if not ('[#1] INFO:Eval' in line or '[#1] INFO:Minization' in line 
	or '1  alpha' in line 
 	or '2  background_yield' in line  
  	or '3  efficiency_bkg' in line  
	or '4  efficiency_sig' in line   
	or '5  fracCB' in line     
	or '6  meanCB' in line      
	or '7  n' in line           
	or '8  sigma' in line     
	or '9  sigma2' in line    
	or '10  signal_yield' in line 
	or '11  tau' in line       
	or '12  tauF' in line):
 	print (line)  
