Browse M/DST interactively:
	lb-run Bender/latest bender 00035742_00000002_1.allstreams.dst
	lb-run Bender/latest dst-dump -f -n 100 00035742_00000002_1.allstreams.dst

RooHist *pullHist = frame->pullHist();
RooPlot* pullFrame = m_observables->get(var)->frame();
            pullFrame->addPlotable(pullHist,"X0 B");
(pullFrame->getAttFill())->SetFillColor(kBlue);


# More room for algorithm names
MessageSvc().Format = "% F%100W%S%7W%R%T %0W%M" 

#Run DaVinci locally

$ lb-run DaVinci/v42r6p1 gaudirun.py ntuple_options.py

### Get Catalog

lb-run LHCbDIRAC dirac-bookkeeping-genXMLCatalog --Options=MC_2016_27163002_Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping28NoPrescalingFlagged_ALLSTREAMS.DST.py --Catalog=myCatalog.xml


from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = [ "xmlcatalog_file:/path/to/myCatalog.xml" ]

