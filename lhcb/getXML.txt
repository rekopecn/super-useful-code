To avoid filling up your AFS quota with DST files, you can also pass Gaudi an XML catalog such that it can access them remotely.

First generate the XML catalog with

(probably need to do) LbLogin -c x86_64-slc6-gcc49-opt


lb-run LHCbDIRAC dirac-bookkeeping-genXMLCatalog --Options=MC_2016_27163002_Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09b_Trig0x6138160F_Reco16_Turbo03_Stripping28NoPrescalingFlagged_ALLSTREAMS.DST.py --Catalog=myCatalog.xml


and add

from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = [ "xmlcatalog_file:/path/to/myCatalog.xml" ]



https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbDiracBKCLI#dirac_bookkeeping_genXMLCatalog
https://lhcb.github.io/starterkit-lessons/first-analysis-steps/files-from-grid.html
