k = Job(application=DaVinci(version='v40r2'))
k.backend = Dirac()
k.name = 'Steve'
k.inputdata =k.inputdata = k.application.readInputData('./MC_2012_27163003_Beam4000GeV2012MagDownNu2.5Pythia8_Sim08e_Digi13_Trig0x409f0045_Reco14a_Stripping20NoPrescalingFlagged_ALLSTREAMS.DST.py')
k.application.optsfile = './ntuple_options.py'
k.outputfiles = [MassStorageFile('DVntuple.root')]
k.submit()


### Using the Shell from IPython

#IPython lets you execute shell commands from within the ganga session. This means you can list the contents of a directory without leaving ganga by typing !ls /tmp/. This will list the contents of the /tmp directory. In our case we can use this to list the contents of the job output directory with !ls $output as we stored the path in the variable output
