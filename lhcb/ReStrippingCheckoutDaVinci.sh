#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh;

E_NO_ARGS=65
if [ $# -eq 1 ]  # Must have five command-line args to demo script.
then
   year=$1	
   echo "Building DaVinci for $year"
else
  echo "Please invoke this script with one command-line argument in the following format:"
  echo "./checkoutDV.sh YEAR"
  exit $E_NO_ARGS
fi


if  [ "$year" = "2018" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/32
then
        mkdir -p 2018
	cd 2018
        mkdir -p S34r0p1
	cd S34r0p1

	export CMTCONFIG=x86_64-slc6-gcc62-opt
	LbLogin.sh	

	lb-dev DaVinci/v44r7
	cd DaVinciDev_v44r7
	git lb-use Stripping
	git lb-checkout Stripping/2018-patches Phys/StrippingSelections
	git lb-checkout Stripping/2018-patches Phys/StrippingSettings
	#make
fi


if  [ "$year" = "2017" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/34
then
        mkdir -p 2017
	cd 2017
        mkdir -p S29r2p1
	cd S29r2p1

	export CMTCONFIG=x86_64-slc6-gcc62-opt
	LbLogin.sh	

	lb-dev DaVinci/v42r8p1
	cd DaVinciDev_v42r8p1
	git lb-use Stripping 
	git lb-checkout Stripping/2017-patches Phys/StrippingSelections
	git lb-checkout Stripping/2017-patches Phys/StrippingSettings
	#make

fi


if  [ "$year" = "2016" ]; #
then
	echo "Not available yet"
fi


if  [ "$year" = "2015" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/33
then
        mkdir -p 2015
	cd 2015
        mkdir -p S24r2
	cd S24r2

	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	lb-dev DaVinci/v38r1p7
	cd DaVinciDev_v38r1p7
	git lb-use Stripping 
	git lb-checkout Stripping/stripping24-patches Phys/StrippingSelections
	git lb-checkout Stripping/stripping24-patches Phys/StrippingSettings
	#make
fi


if  [ "$year" = "2012" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/31
then
	cd 2012
        mkdir -p S21r1p2
	cd S21r1p2

	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	lb-dev DaVinci/v39r1p1
	cd DaVinciDev_v39r1p1
	git lb-use Stripping 
	git lb-checkout Stripping/stripping21-patches Phys/StrippingSelections
	git lb-checkout Stripping/stripping21-patches Phys/StrippingSettings
	#make
fi


### 2011 is setup the same as 2012!!!
if  [ "$year" = "2011" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/31
then
        mkdir -p 2011
	cd 2011
        mkdir -p S21r0p2
	cd S21r0p2

	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	lb-dev DaVinci/v39r1p1
	cd DaVinciDev_v39r1p1
	git lb-use Stripping 
	git lb-checkout Stripping/stripping21-patches Phys/StrippingSelections
	git lb-checkout Stripping/stripping21-patches Phys/StrippingSettings
	#make
fi



