# TO LOAD Tuple_Turbo.py, NEED TO ADD THIS LOCATION TO PYTHON PATH
import os,sys
sys.path.append(os.environ['TRACKCALIBPRODUCTIONROOT']+'/python/TrackCalibProduction')

#from new_tuple import set_NewTuples
from Tuple_Turbo import set_Tuples


#######################
### INPUT           ###
#######################
datatype = "2017"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo_test.py
sample = "Data"          # DATA, MAG UP OR MAG DOWN
from GaudiConf import IOHelper
#IOHelper().inputFiles(['/afs/cern.ch/user/x/xuliu/workdir/public/Data/17MDTurboCalib2.fullturbo.dst'], clear=True)
IOHelper().inputFiles([
'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000476_2.fullturbo.dst',
'root://ccdcacli265.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000217_2.fullturbo.dst',
'root://ccdcacli264.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000194_2.fullturbo.dst',
'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000136_2.fullturbo.dst',
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000172_2.fullturbo.dst',
#'root://ccdcacli265.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000345_2.fullturbo.dst',
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000477_2.fullturbo.dst',
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000511_2.fullturbo.dst',
#'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000377_2.fullturbo.dst',
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000138_2.fullturbo.dst',
#'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000218_2.fullturbo.dst',
#'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000108_2.fullturbo.dst',
#'root://clhcbstager.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000266_2.fullturbo.dst?svcClass=lhcbDst',
#'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00066593/0000/00066593_00000616_2.fullturbo.dst',
], clear=True)


# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype,sample,MDST=False,Test=False)


