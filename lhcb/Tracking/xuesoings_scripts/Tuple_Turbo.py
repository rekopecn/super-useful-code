# Import needed files ############################
from Gaudi.Configuration import * 
from Configurables import DaVinci, TupleToolTagging, CondDB

from Configurables import DecayTreeTuple, LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool
from Configurables import TupleToolTrigger, TupleToolTISTOS, TupleToolDecay,Hlt2TriggerTisTos
from Configurables import TupleToolMCTruth,MCDecayTreeTuple,TupleToolTrackInfo, TupleToolGeometry,TupleToolKinematic
from Configurables import FilterDesktop#, LokiTool

from PhysSelPython.Wrappers import DataOnDemand, Selection, MergedSelection, SelectionSequence # FOR CopyAndMatchCombination
from Configurables import CopyAndMatchCombinationTC

from TeslaTools import TeslaTruthUtils  
from PhysConf.Filters import LoKi_Filters

from DecayTreeTuple.Configuration import *

################################################################################
## Configuration of the MicroDST writer                                       ##
################################################################################

from PhysSelPython.Wrappers import MultiSelectionSequence
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter, MicroDSTWriter ,stripMicroDSTStreamConf,
                       stripMicroDSTElements, stripCalibMicroDSTStreamConf,
                       stripDSTElements, stripDSTStreamConf)
from DSTWriters.streamconf import OutputStreamConf
from Configurables import OutputStream

def stripMicroDSTStreamConf( pack = True,
                             isMC = False,
                             selectiveRawEvent = False,
                             killTESAddressHistory = True ) :
    eItems = [ '/Event/Rec/Header#1',
               '/Event/Rec/Status#1',
               '/Event/Rec/Summary#1',
               '/Event/Trigger/RawEvent#1',
               #'/Event/PersistReco/RawEvent#1',
               '/Event/Turbo#99', 
               #'/Event/Turbo/Rec/Summary#99', 
               ]
    #if pack :
    #    eItems += ['/Event/Strip/pPhys/DecReports#1']
    #else :
    #    eItems += ['/Event/Strip/Phys/DecReports#1']
    if isMC :
        eItems += ["/Event/MC/Header#1",
                   "/Event/MC/DigiHeader#1",
                   "/Event/Gen/Header#1"]
    return OutputStreamConf( streamType    = OutputStream,
                             filePrefix    = '.',
                             fileExtension = '.mdst',
                             extraItems    = eItems,#['/Event/Rec/Summary#1'],#eItems,
                             vetoItems     = [],
                             selectiveRawEvent = selectiveRawEvent,
                             killTESAddressHistory = killTESAddressHistory )




def configureMicroDSTwriter( name, prefix, sequences):
  # Configure the dst writers for the output
  pack = True

  microDSTwriterInput = MultiSelectionSequence ( name , Sequences = sequences )

  # Configuration of MicroDST
  # per-event an per-line selective writing of the raw event can be active (selectiveRawEvent=True)
  mdstStreamConf = {'default':stripMicroDSTStreamConf(pack=pack, selectiveRawEvent = False)}
  mdstElements   = {'default':stripMicroDSTElements(pack=pack)}

  dstWriter = SelDSTWriter( "MyDSTWriter",
                            StreamConf = mdstStreamConf,
                            MicroDSTElements = mdstElements, 
                            OutputFileSuffix = prefix,
                            SelectionSequences = [microDSTwriterInput]
                          )

  from Configurables import StoreExplorerAlg
  return [microDSTwriterInput.sequence(), 
              dstWriter.sequence()]



def set_Tuples(datatype,sample,MDST=False,Test=False):
    #######################
    ### CONFIG          ###
    #######################
    charge_list = ["Plus","Minus"]
    method_list = ["VeloMuon","MuonTT","Downstream"]
    name_dict = {
            "VeloMuon"  : "T",
            "MuonTT"    : "Long",
            "Downstream": "Velo"
            }
    trig_year = {       "2015":[""], # TRIGGER LINES PER YEAR, CHANGED IN 2016
                    "2016":["LowStat","HighStat"],
                    "2017":["LowStat","HighStat"] # ADD XUESONG
                } 

    # Interesting trigger lines 
    tlist = ["L0HadronDecision","L0MuonDecision","L0ElectronDecision","Hlt1DiMuonHighMassDecision","Hlt1DiMuonLowMassDecision","Hlt1TrackMuonDecision","Hlt1TrackAllL0Decision",'Hlt1TrackMVADecision','Hlt1TrackMuonMVADecision']
    Hlt2_lines = [] 
    for method in method_list:
        for charge in charge_list:
            for trig in trig_year[datatype[:4]]:
                Hlt2_lines += [ 'Hlt2TrackEffDiMuon'+method+charge+trig+'TaggedTurboCalib',
                                'Hlt2TrackEffDiMuon'+method+charge+trig+'MatchedTurboCalib'     
                ]               
    for Hlt2_line in Hlt2_lines: tlist+=[Hlt2_line+"Decision"]

    if Test:
        # COND-DB and DDDB for MC
        cond_db_dict = {
                        "2015":       "sim-20160606-vc-m{0}100",
                        "2015_EM":    "sim-20161124-2015early-vc-m{0}100",
                        "2015_5TeV":  "sim-20161124-2015at5TeV-vc-m{0}100",
                        "2015_Sim09b":"sim-20161124-vc-m{0}100",

                        "2016":       "sim-20160820-vc-m{0}100",
                        "2016_Sim09b":"sim-20161124-2-vc-m{0}100",

				"2017":	 "sim-20171127-vc-m{0}100"  #ADD XUESONG
                        } 

        dd_db_dict = {      "2015":"dddb-20150724",
                        "2016":"dddb-20150724",
                        "2017":"dddb-20171126" #ADD XUESONG
                        } 

    #######################
    ### TUPLE CONFIG    ###
    #######################
    tuple_list = []

    filterSequences, matchingSequences,reviveSequences = [],[],[]

    inputSequences = [] # FOR WRITING OUT THE INFO FOR TupleToolTrackEffMatch TO MDST


    if not MDST: 

        # SEQUENCE TO SAVE STDALLNOPIDSMUONS
        #reviveSequence = SelectionSequence("fs_stdMuons",  TopSelection = DataOnDemand("/Event/Phys/StdAllNoPIDsMuons/Particles"))
        #reviveSequences.append(reviveSequence)
        #tuple_list.append(reviveSequence)                     


        matchingLocation = {
            "mu+"          :  "/Event/Phys/StdAllNoPIDsMuons/Particles",
            "pi+"          :  "/Event/Phys/StdAllNoPIDsPions/Particles",
            "K+"           :  "/Event/Phys/StdAllNoPIDsKaons/Particles",
            "p+"           :  "/Event/Phys/StdAllNoPIDsProtons/Particles",
            "e+"           :  "/Event/Phys/StdAllNoPIDsElectrons/Particles",
        }
    else:
        matchingLocation = {
                    "mu+"          :  "/Event/TRKCALIB/Phys/StdAllNoPIDsMuons/Particles",#Match_selection"+method+charge+"/Particles",
                #    "pi+"          :  "/Event/TRKCALIB/Phys/Match_selection"+method+charge+"/Particles",
                #    "K+"           :  "/Event/TRKCALIB/Phys/Match_selection"+method+charge+"/Particles",
                #    "p+"           :  "/Event/TRKCALIB/Phys/Match_selection"+method+charge+"/Particles",
                #    "e+"           :  "/Event/TRKCALIB/Phys/Match_selection"+method+charge+"/Particles",
                }

    for method in method_list:
        for charge in charge_list:

            # IF MDST, MATCHING LOCATION DEPENDS ON METHOD, CHARGE, AS IT IS BASED ON THE MATCHING SELECTION DONE ON THE DST
            if MDST: 

            
                input_list = ["/Phys/filter_selection"+method+charge+"/Particles"]

            else:

                # DEFINE LOCATIONS
                input_list = ["/Event/Turbo/Rec/Summary"] 

                if "2015" in datatype: 
                    input_list += ["/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"TaggedTurboCalib/Particles"
                                     ,"/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"MatchedTurboCalib/Particles"]
                else:
                    input_list += ["/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatTaggedTurboCalib/Particles"
                                     ,"/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                                     ,"/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatTaggedTurboCalib/Particles"
                                     ,"/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"]


                # INPUT SELECTION TO HAVE MATCHED INFORMATION IN .MDST
                inputSelection_list =  [ DataOnDemand(line) for line in input_list if "Matched" in line]
                for i in range(len(inputSelection_list)):
                    inputSequence = SelectionSequence("Seq_input_selection"+method+charge+str(i),TopSelection = inputSelection_list[i])
                    inputSequences.append(inputSequence)
                    tuple_list.append(inputSequence)
 

            # INPUT SELECTION FOR MAKING THE NTUPLE
            inputSelection = MergedSelection ( "input_selection"+method+charge ,
                  RequiredSelections = [ DataOnDemand(line) for line in input_list],)

            # FILTER REQUIRED FOR MC
            selection = Selection("filter_selection"+method+charge,
                RequiredSelections = [ inputSelection ],
                Algorithm = FilterDesktop("alg_selection"+method+charge, Code = 'HASVERTEX' ))

            filterSequence = SelectionSequence("Seq_selection"+method+charge,
              TopSelection = selection)

            filterSequences.append(filterSequence)


            # SEQUENCE FOR MATCHING OF OFFLINE AND ONLINE TRACKS
            matchingSel = Selection("Match_selection"+method+charge,
                Algorithm = CopyAndMatchCombinationTC ( "MatchAlg_selection"+method+charge ),
                RequiredSelections = [selection]
                )

            matchingSequence = SelectionSequence ( "SeqMatch_selection"+method+charge,
                  TopSelection = matchingSel )

            matchingSequences.append(matchingSequence)

            



            tuple = DecayTreeTuple("Tuple"+name_dict[method]+charge)
            # IF RUNNNING ON TESLA AFTER v1r0 OR THE HEAD AFTER 2/12/2014
            # PLEASE UPDATE THE TES LOCATION WITH /Event/Tesla/HLTLINENAME/Particles
            #tuple.Inputs = [ "/Event/Turbo/Particles" ]
            tuple.Inputs = [filterSequence.outputLocation()] 
            if MDST: tuple.RootInTES = "/Event/TRKCALIB/"

            tuple.ToolList =  [
                "TupleToolGeometry"
                , "TupleToolKinematic"
                , "TupleToolTISTOS"
                , "TupleToolTrigger"
                , "TupleToolPid"
                , "TupleToolRecoStats"
                , "TupleToolEventInfo"
                , "TupleToolBeamSpot"
                , "TupleToolL0Data"
            ]
            if "MC" in sample:
                tuple.ToolList+= [
                     "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                ]


            tuple.Decay = "J/psi(1S) -> ^mu+ ^mu-" 

            eventTool = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")

            # FOR EVENT WIDE INFO
            eventVariables = { 
            "nPVs_Brunel"              : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999)"
           , "nPVs_Turbo"              : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999, '/Event/Turbo/Rec/Summary')"
            , "nLongTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nLongTracks       , -9999)"
            , "nLongTracks_Turbo"       : "RECSUMMARY( LHCb.RecSummary.nLongTracks       , -9999, '/Event/Turbo/Rec/Summary')"
            , "nDownstreamTracks_Brunel" : "RECSUMMARY( LHCb.RecSummary.nDownstreamTracks , -9999)"
            , "nDownstreamTracks_Turbo" : "RECSUMMARY( LHCb.RecSummary.nDownstreamTracks , -9999, '/Event/Turbo/Rec/Summary')"
          #  , "nUpstreamTracks"   : "RECSUMMARY( LHCb.RecSummary.nUpstreamTracks   , -9999)"
            , "nVeloTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nVeloTracks       , -9999)"
            , "nVeloTracks_Turbo"       : "RECSUMMARY( LHCb.RecSummary.nVeloTracks       , -9999, '/Event/Turbo/Rec/Summary')"
          #  , "nTTracks"          : "RECSUMMARY( LHCb.RecSummary.nTTracks          , -9999)"
          #  , "nBackTracks"       : "RECSUMMARY( LHCb.RecSummary.nBackTracks       , -9999)"
            , "nTracks_Brunel"           : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999)"
            , "nTracks_Turbo"           : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999, '/Event/Turbo/Rec/Summary')"
            , "nRich1Hits_Brunel"        : "RECSUMMARY( LHCb.RecSummary.nRich1Hits        , -9999)"
            , "nRich1Hits_Turbo"        : "RECSUMMARY( LHCb.RecSummary.nRich1Hits        , -9999, '/Event/Turbo/Rec/Summary')"
            , "nRich2Hits_Brunel"        : "RECSUMMARY( LHCb.RecSummary.nRich2Hits        , -9999)"
            , "nRich2Hits_Turbo"        : "RECSUMMARY( LHCb.RecSummary.nRich2Hits        , -9999, '/Event/Turbo/Rec/Summary')"
            , "nVeloClusters_Brunel"     : "RECSUMMARY( LHCb.RecSummary.nVeloClusters     , -9999)"
            , "nVeloClusters_Turbo"     : "RECSUMMARY( LHCb.RecSummary.nVeloClusters     , -9999, '/Event/Turbo/Rec/Summary')"
          #  , "nITClusters"       : "RECSUMMARY( LHCb.RecSummary.nITClusters       , -9999)"
          #  , "nTTClusters"       : "RECSUMMARY( LHCb.RecSummary.nTTClusters       , -9999)"
          #  , "nUTClusters"       : "RECSUMMARY( LHCb.RecSummary.nUTClusters       , -9999)"
          #  , "nOTClusters"       : "RECSUMMARY( LHCb.RecSummary.nOTClusters       , -9999)"
          #  , "nFTClusters"       : "RECSUMMARY( LHCb.RecSummary.nFTClusters       , -9999)
            , "nSPDhits_Brunel"          : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999)"
            , "nSPDhits_Turbo"          : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999, '/Event/Turbo/Rec/Summary')"
        #    , "nMuonCoordsS0"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS0     , -9999)"
        #    , "nMuonCoordsS1"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS1     , -9999)"
        #    , "nMuonCoordsS2"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS2     , -9999)"
        #    , "nMuonCoordsS3"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS3     , -9999)"
        #    , "nMuonCoordsS4"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS4     , -9999)"
            , "nMuonTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nMuonTracks       , -9999)"
            , "nMuonTracks_Turbo"       : "RECSUMMARY( LHCb.RecSummary.nMuonTracks       , -9999, '/Event/Turbo/Rec/Summary')"
          }
          
            eventTool.VOID_Variables = eventVariables
            eventTool.Preambulo = [
              "from LoKiTracks.decorators import *",
              "from LoKiCore.functions import *"
            ]
            tuple.addTool( eventTool )

            LoKiTool = tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
            LoKiTool.Variables = { "ETA" : "ETA",
                                   "PHI" : "PHI" }

            tuple.addTool(TupleToolTrigger, name="TupleToolTrigger")
            tuple.addTool(TupleToolTISTOS,  name="TupleToolTISTOS" )
            # Get trigger info
            tuple.TupleToolTrigger.Verbose = True
            tuple.TupleToolTrigger.TriggerList = tlist
            tuple.TupleToolTISTOS.Verbose = True
            tuple.TupleToolTISTOS.TriggerList = tlist
            tuple.TupleToolTISTOS.TUS = True

            # Add verbose Track info
            track_tool = tuple.addTupleTool('TupleToolTrackInfo')
            track_tool.Verbose = True

            # Add and configure TrackEff tool
            trackeffTool = tuple.addTupleTool('TupleToolTrackEffMatch')
            trackeffTool.MC = ("MC" in sample)
            if MDST:
                if '2015' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"MatchedTurboCalib/Particles"
                elif '2016' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                    trackeffTool.MatchedLocation2 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"
                elif '2017' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                    trackeffTool.MatchedLocation2 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"
            else:
                if '2015' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"MatchedTurboCalib/Particles"
                elif '2016' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                    trackeffTool.MatchedLocation2 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"
                elif '2017' in datatype[:4]:
                    trackeffTool.MatchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                    trackeffTool.MatchedLocation2 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"



            trackeffTool.OutputLevel = 5 #ERROR

            # ON XDST OR LDST THE FOLLOWING LINES ARE NEEDED FOR MCTRUTH
            if "MC" in sample:
                relations = [TeslaTruthUtils.getRelLoc("")] 
                TeslaTruthUtils.makeTruth(tuple, relations, [ "MCTupleToolKinematic", "MCTupleToolReconstructed" ])

            # IF RUNNING ON TESLA AFTER v1r0 OR THE HEAD AFTER 2/12/2014
            # THE PV LOCATIONS HAVE BEEN MOVED TO NOT COINCIDE WITH OFFLINE
            # LOCATIONS, THEREFORE THE FOLLOWING LINES ARE NEEDED
            tuple.WriteP2PVRelations = False
            if MDST:    tuple.InputPrimaryVertices = "/Turbo/Primary"
            else:       tuple.InputPrimaryVertices = "/Event/Turbo/Primary"


            ttt_conf = Hlt2TriggerTisTos()
            if method=='MuonTT':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            if method=='VeloMuon':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracTT = 0.
                ttt_conf.TISFracTT = 0.
            if method=='Downstream':
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            ttt_conf.TOSFracEcal = 0.
            ttt_conf.TOSFracHcal = 0.
            ttt_conf.TISFracEcal = 0.
            ttt_conf.TISFracHcal = 0.
            tuple.TupleToolTISTOS.addTool(ttt_conf)


            tuple_list.append(matchingSequence)

            tuple_list.append(tuple)

            # ADD THE ONLINE/OFFLINE MATCHING AND ALL ASSOCIATED TOOLS
            matcher = tuple.addTupleTool ( "TupleToolTwoParticleMatching/Matcher_" + method+charge )
            matcher.ToolList = []
            matcher.Prefix = ""; matcher.Suffix = "_Brunel"
            matcher.MatchLocations = matchingLocation


            track_tool_match = TupleToolTrackInfo('track_match_'+method+charge) 
            track_tool_match.Verbose = True 
            matcher.addTool(track_tool_match)
            matcher.ToolList += ["TupleToolTrackInfo/track_match_"+method+charge]

            geome_tool_match = TupleToolGeometry('geome_match_'+method+charge) 
            matcher.addTool(geome_tool_match)
            matcher.ToolList += ["TupleToolGeometry/geome_match_"+method+charge]
    
            kinem_tool_match = TupleToolKinematic('kinem_match_'+method+charge) 
            matcher.addTool(kinem_tool_match)
            matcher.ToolList += ["TupleToolKinematic/kinem_match_"+method+charge]

            lokimatchedtool = LoKi__Hybrid__TupleTool("Loki_match_"+method+charge)
            lokimatchedtool.Variables = { "ETA" : "ETA",   "PHI" : "PHI" }
            matcher.addTool(lokimatchedtool)
            matcher.ToolList += ["LoKi::Hybrid::TupleTool/Loki_match_"+method+charge]


            tistos_match    = TupleToolTISTOS("tistos_match_"+method+charge )
            # Get trigger info
            tistos_match.Verbose = True
            tistos_match.TriggerList = tlist
            tistos_match.TUS = True

            ttt_conf = Hlt2TriggerTisTos("hlt2_match_"+method+charge)
            if method=='MuonTT':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            if method=='VeloMuon':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracTT = 0.
                ttt_conf.TISFracTT = 0.
            if method=='Downstream':
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            ttt_conf.TOSFracEcal = 0.
            ttt_conf.TOSFracHcal = 0.
            ttt_conf.TISFracEcal = 0.
            ttt_conf.TISFracHcal = 0.
            tistos_match.addTool(ttt_conf)


  
            matcher.addTool(tistos_match)


            matcher.ToolList+= ["TupleToolTISTOS/tistos_match_"+method+charge]







    if "MC" in sample:
        #MC TOOL  ==========
        mctuple = MCDecayTreeTuple("MCTuple")
        mctuple.Decay = "[B0 =>  ^(J/psi(1S) => ^mu+ ^mu-) X ]CC "
        mctuple.ToolList =  ["MCTupleToolKinematic"
                             ,"MCTupleToolEventType"
                             ,"MCTupleToolReconstructed"
                             ,"MCTupleToolHierarchy"
                             ,"TupleToolEventInfo"
                ]
        # ============
        tuple_list.append(mctuple)


    if not MDST and not "MC" in sample: tuple_list += configureMicroDSTwriter ( "TRKCALIB","", inputSequences+ filterSequences+matchingSequences)
      


    # DaVinci config
    if "MC" in sample:
        if Test:
            DaVinci().CondDBtag = cond_db_dict[datatype].format(sample.split('_')[1].lower()[0])
            DaVinci().DDDBtag   = dd_db_dict[datatype[:4]]
        DaVinci().InputType = 'MDST'

    else:
        if MDST:     DaVinci().InputType = 'MDST'
        if not MDST: DaVinci().InputType =  'DST'
        if Test: db = CondDB  ( LatestGlobalTagByDataType = datatype[:4] )



    DaVinci().DataType    = datatype[:4]
    DaVinci().TupleFile = 'Tuple_{0}_{1}.root'.format(datatype,sample)
    DaVinci().Lumi = not ("MC" in sample)
    DaVinci().Simulation = ("MC" in sample)
    DaVinci().UserAlgorithms = tuple_list 
    DaVinci().Turbo = True  #XUESONG
    DaVinci().RootInTES = '/Event/Turbo' #XUESONG
    if Test:  DaVinci().EvtMax = 1000
    

    # TO SPEED UP, ONLY CONSIDER EVENTS PASSING THE TRIGGER LINES DEFINED FOR THE TRACKING EFFICIENCY
    if datatype[:4] == '2015':
        filter = LoKi_Filters (
                HLT_Code  = "HLT_PASS_RE('Hlt2TrackEffDiMuon.*Decision')"
                )
    else:
        filter = LoKi_Filters (
                HLT2_Code = "HLT_PASS_RE('Hlt2TrackEffDiMuon.*Decision')"
                )
    filterseq = filter.sequence('PreFilter')
    DaVinci().EventPreFilters = [filterseq]

    if Test:
        # CONFIGURE PROD CONF
        from ProdConf import ProdConf
        filename = "{0}_{1}".format(sample,datatype)

        test_folder = 'root://eoslhcb.cern.ch//eos/lhcb/user/m/mmulder/TAeff/'
        test_file = {
          "MC_Up_2015_Sim09b"  :"MC_2015_Up/00058366_00000001_1.dst",
          "MC_Down_2015_Sim09b":"MC_2015_Down/00058363_00000001_1.dst",
          "MC_Up_2015"         :"MC_2015_Up/00058366_00000001_1.dst",
          "MC_Down_2015"       :"MC_2015_Down/00058363_00000001_1.dst",
          "MC_Up_2015_EM"      :"MC_2015_Up/00058366_00000001_1.dst", # UPDATE
          "MC_Down_2015_EM"    :"MC_2015_Down/00058363_00000001_1.dst", # UPDATE
          "MC_Up_2015_5TeV"    :"MC_2015_Up/00058366_00000001_1.dst", # UPDATE
          "MC_Down_2015_5TeV"  :"MC_2015_Down/00058363_00000001_1.dst", # UPDATE

          "MC_Up_2016_Sim09b"  :"MC_2016_Up/00061062_00000001_1.dst",
          "MC_Down_2016_Sim09b":"MC_2016_Down/00058357_00000001_1.dst",
          "MC_Up_2016"         :"MC_2016_Up/00061062_00000001_1.dst",
          "MC_Down_2016"       :"MC_2016_Down/00058357_00000001_1.dst",

          "Data_2016_Sim09b"   :"Data_2016/00052098_00000010_2.fullturbo.dst"
          }
     
        if MDST: 
            input_files  = ['/afs/cern.ch/user/m/mmulder/TAeff/mdst_test/TRKCALIB_DaVinci.mdst']
            output_files = ["trkcalib.root"]
        else:
            input_files  = [test_folder+test_file[filename]]
            output_files = ["TRKCALIB.mdst","trkcalib.root"]


        ProdConf(
          TCK='0x104024e',
          NOfEvents=-1,# OVERWRITTEN BY DAVINCI
          OptionFormat='WGprod',
          HistogramFile=filename+'_Hist.root',
          DDDBTag='', # OVERWRITTEN BY DV
          CondDBTag='', # OVERWRITTEN BY DV
          ProcessingPass='{0}'.format(filename),
          AppVersion='v15r0',
          XMLSummaryFile=filename+'_Test.xml',
          Application='Erasmus',
          OutputFilePrefix='',

          XMLFileCatalog='pool_xml_catalog.xml',
          InputFiles=input_files,
          OutputFileTypes=output_files   
        )


            






