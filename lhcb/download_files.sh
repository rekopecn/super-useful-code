#!/bin/bash
#
# #first initialize connection to Dirac by using:
# lhcb-proxy-init
#
#./jobloader.sh JOB_ID YEAR MAGNET
#1352 up long, 1353 up velo, 1354 down long, 1355 down velo

PathToGangaFolder='/afs/cern.ch/user/r/rekopecn/gangadir/workspace/rekopecn/LocalXML'
NameTagForOutputFiles='Track'
#NameTagForOutputFiles='B2KstJpsi'
NameOfRootfileInGangaOutput='sim09D_valid'

E_NO_ARGS=65
if [ $# -eq 4 ]  # Must have five command-line args to demo script.
then
   job=$1
   datatype=$2
   year=$3
   magnet=$4
   echo "Collecting $NameTagForOutputFiles for $year$magnet"
else
  echo "Please invoke this script with one or more command-line arguments in the following format:"
  echo "./download_ganga_files.sh JOB DATATYPE YEAR MAGNET"
  exit $E_NO_ARGS
fi

### Create folders where the tuples will be downloaded to
cd ..
if [ -e data ]
then
	echo "folder data/ found!"
else
	echo "creating folder data/"
	mkdir data
fi


if [ -e data/$datatype ]
then
	echo "folder data/$datatype found!"
else
	echo "creating folder data/$datatype"
	mkdir data/$datatype
fi

if [ -e data/$datatype/$year$magnet ]
then
	echo "folder data/$datatype/$year$magnet found!"
else
	echo "creating folder data/$datatype/$year$magnet"
	mkdir data/$datatype/$year$magnet
fi

### Nuber of subjobs to look for
maxNumberOfSubjobs=25

echo "Collecting $NameTagForOutputFiles for $datatype/$year$magnet files..."
counter=0
prefix="root->"
apperfix=":::"

while [ $counter -lt $maxNumberOfSubjobs ]
do
   outfile=$year$magnet\_$NameTagForOutputFiles\_$counter.trkcalib.root
   #echo $outfile
   if [ -e data/$datatype/$year$magnet/$outfile ]
   then
       echo " $outfile is already existig, moving to next line..."
   else
       if [ -e  $PathToGangaFolder/$job/$counter/output/\_\_postprocesslocations\_\_ ]
       then
    	  #while read line
    	  #do
	  #if  [ "$magnet" = "Down" ]; #For whatever reason Up is saved with dummy rootfile being second, Down with dummy being first
	  #then
          #	line=$(sed -n "2p" $PathToGangaFolder/$job/$counter/output/\_\_postprocesslocations\_\_)  #Read only the second line of the file
		#echo $line
          #else 
          read line < $PathToGangaFolder/$job/$counter/output/\_\_postprocesslocations\_\_
	  #fi
          line="${line#*$prefix}"
          line="${line%%$apperfix*}"
          echo "   -> Copying $line to data/$datatype/$year$magnet/$outfile..."
          lb-run LHCbDirac dirac-dms-get-file $line
          NameOfRootfileInGangaOutput="${line##*/}"
          #done < $PathToGangaFolder/$job/$counter/output/\_\_postprocesslocations\_\_        
    	  mv $NameOfRootfileInGangaOutput data/$datatype/$year$magnet/$outfile
          #else echo "file $PathToGangaFolder/$counter/\_\_postprocesslocations\_\_ not found!"
          echo "  Done."
        fi
   fi
   let counter=$counter+1
done
echo "Finished downloading $datatype/$year$magnet."
