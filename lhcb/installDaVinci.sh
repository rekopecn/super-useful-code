#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh;

E_NO_ARGS=65
if [ $# -eq 3 ]  # Must have three command-line args to demo script.
then
   	version=$1	
	analysis=$2
	branch=$3
else
  	echo "Please invoke this script with three command-line arguments in the following format:"
	echo "./installDaVinci version analyisVersion branch"
	exit $E_NO_ARGS
fi

#change in case of need, to lazy to parse the version	
export CMTCONFIG=x86_64-slc6-gcc62-opt
LbLogin.sh	

lb-dev DaVinci/$1
cd ./DaVinciDev_$1
git lb-use Analysis
git lb-checkout Analysis/$2 Phys/$3

make configure
make install
