
#run by lb-run DaVinci v40r2 gaudirun.py ntuple_options.py


from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

#Stream and stripping line we want to use
stream = 'AllStreams'
line =  'D2hhCompleteEventPromptDst2D2RSLine'

# Create an ntuple to capture D*+ decays from the StrippingLine line
dtt = DecayTreeTuple('TupleDstToD0pi_D0ToKpi') #saves to a folder named 'TupleDstToD0pi_D0ToKpi'
	#dtt represents the ntuple
dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream,line)]
	#specifies where DecayTreeTuple should look for particles
dtt.Decay = '[D*(2010)+ -> (D0 -> K-pi+) pi+]CC'
	# specifies what decay we would like to have in our ntuple. If there are no particles at the Input location, or the Decay string doesn't match any particles at that location, the ntuple will not be filled. There is a special syntax for the Decay attribute string, commonly called 'decay descriptors', that allow a lot of flexibility with what you accept. For example, D0 -> K- X+ will match any D0 decay that contains one negatively charged kaon and one positively charged track of any species. More information the decay descriptor syntax can be found on the LoKi decay finders TWiki page.

from Configurables import DaVinci

# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2012'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().CondDBtag = 'sim-20130522-1-vc-md100'
DaVinci().DDDBtag = 'dddb-20130929-1'
#The CondDBtag and DDDBtag attributes specify the exact detector conditions that the Monte Carlo was generated with. Specifying these tags is important, as without them you can end up with the wrong magnet polarity value in your ntuple, amongst other Bad Things. You can find the values for these tags in the bookkeeping file we downloaded earlier. Generally, the CondDB and DDDB tags are different for each dataset you want to use, but will be the same for all DSTs within a given dataset. When using simulated data, always find out what the database tags are for your dataset! For real collision data, you shouldn't specify these tags, as the default tags are the latest and greatest, so just remove those lines from the options file.

from GaudiConf import IOHelper

#Use the local input data
IOHelper().inputFiles([
    '/afs/cern.ch/user/r/rekopecn/test/00035742_00000002_1.allstreams.dst'
], clear=True)


#---------------------------------------------------
#---------------------------------------------------
#---------------------------------------------------


#The default tools added in DecayTreeTuple are:
    #TupleToolKinematic, which fills the kinematic information of the decay.
    #TupleToolPid, which stores DLL and PID information of the particle.
    #TupleToolANNPID, which stores the new NeuralNet-based PID information of the particle.
    #TupleToolGeometry, which stores the geometrical variables (IP, vertex position, etc) of the particle.
    #TupleToolEventInfo, which stores general information (event number, run number, GPS time, etc) of the event.

#track_tool = dtt.addTupleTool('TupleToolTrackInfo')
#track_tool.Verbose = True
#dtt.addTupleTool('TupleToolPrimaries')
#dtt.Decay = '[D*(2010)+ -> (D0 -> K- pi+) pi+]CC'
#dtt.Decay = '[D*(2010)+ -> ^(D0 -> ^K- ^pi+) pi+]CC'

#dtt.addBranches({'Dstar' : '[D*(2010)+ -> (D0 -> K- pi+) pi+]CC'})#,
#                 'D0'    : '[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC',
#                 'Kminus': '[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC',
#                 'piplus': '[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC',
#                 'pisoft': '[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC'})

#dtt.addBranches({'D0'    : '[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC'})
#dtt.D0.addTupleTool('TupleToolPropertime')


#dstar_hybrid = dtt.Dstar.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Dstar')
#d0_hybrid = dtt.D0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_D0')
#pisoft_hybrid = dtt.pisoft.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_PiSoft')


#preamble = [
#    'DZ = VFASPF(VZ) - BPV(VZ)',
#    'TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)'
#]
#dstar_hybrid.Preambulo = preamble
#d0_hybrid.Preambulo = preamble


#dstar_hybrid.Variables = {
#    'mass': 'MM',
#    'mass_D0': 'CHILD(MM, 1)',
#    'pt': 'PT',
#    'dz': 'DZ',
#    'dira': 'BPVDIRA',
#    'max_pt': 'MAXTREE(ISBASIC & HASTRACK, PT, -1)',
#    'max_pt_preambulo': 'TRACK_MAX_PT',
#    'sum_pt_pions': 'SUMTREE(211 == ABSID, PT)',
#    'n_highpt_tracks': 'NINTREE(ISBASIC & HASTRACK & (PT > 1500*MeV))'
#}
#d0_hybrid.Variables = {
#    'mass': 'MM',
#    'pt': 'PT',
#    'dira': 'BPVDIRA',
#    'vtx_chi2': 'VFASPF(VCHI2)',
#    'dz': 'DZ'
#}
#pisoft_hybrid.Variables = {
#    'p': 'P',
#    'pt': 'PT'
#}

#dtt.addBranches({
#    'Dstar': '[D*(2010)+ -> (D0 -> K- pi+) pi+]CC',
#}) 
#dtt.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConsD')
#dtt.Dstar.ConsD.constrainToOriginVertex = True
#dtt.Dstar.ConsD.Verbose = True
#dtt.Dstar.ConsD.daughtersToConstrain = ['D0']




