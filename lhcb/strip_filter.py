from PhysConf.Filters import LoKi_Filters
stripFilter = LoKi_Filters (
    STRIP_Code = " HLT_PASS_RE ('.*FullDSTDiMuonJpsi2MuMuDetachedLine.*')"
)
stripFilterSeq = stripFilter.sequence( "StripFilter" )
# ######################################################################################################
from Configurables import DaVinci
DaVinci().EvtMax    = 100
DaVinci().DataType  = "2016"
DaVinci().PrintFreq = 500
DaVinci().EventPreFilters = [ stripFilterSeq ]

from GaudiConf import IOHelper
io = IOHelper()
##bla = io.outputAlgs( '/eos/lhcb/user/d/decianm/JpsiKsNewPatLLT_1.dst', 'InputCopyStream/inputcopy')
bla = io.outputAlgs( './TESTJpsi5TeV.dst', 'InputCopyStream/inputcopy')
bla[0].AcceptAlgs = DaVinci().EventPreFilters
#
from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = bla )
                            
################################################################

####################################################
# local files for testing go here
####################################################
#IOHelper('ROOT').inputFiles([
    #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/DIMUON.DST/00054259/0000/00054259_00000031_1.dimuon.dst',
#     ])
