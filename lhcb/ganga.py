print (jobs)

def remove_jobs(fr,to):
	for i in range (fr,to+1):
		jobs(i).remove()
	return

def force_fail_jobs(fr,to):
	for i in range (fr,to+1):
		jobs(i).force_status('failed')
	return

def resubmit_failed_subjobs(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
         		#js.backend.settings['BannedSites'] = ['LCG.RAL.uk'] #optional
        		js.resubmit()
	return

def resubmit_failed_subjobs_multiple(fr,to):
	for i in range (fr,to+1):
		resubmit_failed_subjobs(i)
	return


def resubmit_new_subjobs(job_id):
	for js in jobs(job_id).subjobs:
        	if js.status=='new':
			#js.backend.settings['BannedSites'] = ['LCG.CERN.ch'] #optional
                	js.submit()
	return


def resubmit_killed_subjobs(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='killed':
			#js.backend.settings['BannedSites'] = ['LCG.CERN.ch'] #optional
			js.resubmit()
	return

def output_tmp(job_id):
	for sj in jobs(job_id).subjobs:
		print (jobs(job_id).subjobs(sj).outputfiles)
	return

def write_lfn(job_id, output_filename):
	lfns = [] #list of files we want to merge
	for sj in jobs(job_id).subjobs.select(status='completed'):
		for df in sj.outputfiles.get(DiracFile):
			lfn = df.lfn
			if lfn == '':
				print ('Empty LFN for subjob {0}'.format(subjob.id))
				continue
			lfns.append(lfn)
	with open(output_filename, 'w') as f: #with makes sure we have a file to open
		f.writelines('\n'.join(lfns))

def list_subjobs(fr,to):
	for i in range (fr,to+1):
		print (jobs(i).subjobs)
	return

def force_fail_site(job_id,banned_site):
	for sj in jobs(job_id).subjobs:
		if sj.backend.actualCE == banned_site: 	sj.force_status('failed')

def force_fail_running_subjobs(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='running':		
			jobs(job_id).kill()
#			jobs(job_id).force_status('failed')


def resubmit_failed_subjobs_ban(job_id,banned_site):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
				js.backend.settings['BannedSites'] = ['banned_site'] #optional
				js.resubmit()


def resubmit_failed_subjobs_CPU(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
			js.backend.settings['CPUTime']=js.backend.settings['CPUTime']*2
			js.resubmit()

def resubmit_failed_subjobs_CPU_ft_to(fr,to):
	for job_id in range (fr,to+1):
		for js in jobs(job_id).subjobs:
			if js.status=='failed':
				js.backend.settings['CPUTime']=js.backend.settings['CPUTime']*2
				js.resubmit()
	return


def resubmit_failed_subjobs_banSite(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
			js.backend.settings['BannedSites']=[js.backend.actualCE]
			js.resubmit()

def resubmit_failed_subjobs_new_job(job_id,subjob_id):
	js = jobs(job_id).subjobs[subjob_id]
	j = js.copy()
	j.name = str(job_id) + "." + str(subjob_id)
	j.submit()

def resubmit_failed_jobs_new_job(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
	 		resubmit_failed_subjobs_new_job(job_id,subjob_id)

def resubmit_stuck_subjobs_submitting(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='submitting':
			js.force_status('failed')
			js.resubmit()

def reset_backend(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='failed':
			js.backend.reset()

def change_submitted_to_failed(job_id):
	for js in jobs(job_id).subjobs:
		if js.status=='submitted':
			js.force_status('failed')


def print_names(fr,to):
	for job_id in range (fr,to+1):
		print (job_id, jobs(job_id).name)

def delete_files(job_id):
	for sj in jobs(job_id).subjobs: 
		for f in sj.outputfiles.get(DiracFile):
			if f.lfn:
				f.remove()

def delete_files_from_to(fr,to):
	for job_id in range (fr,to+1):
		delete_files(job_id)

#scprit for condition tags from vanya
def getBKInfo ( evttype )  :
    """Get meta-information (paths and tags) from bookeeping databbase 
    - evttype : the event type
    The function returns the list/tuple of production summaries:

    >>> productions = getBKInfo2 ( 13104231 )
    >>> for entry in productions :
    ...   print 'INFORMATION: %s '  % entry 
    ...   path      = entry ['path'     ]      ## 'long path'
    ...   prod_path = entry ['ProductionPath'] ## production path 
    ...   dddbtag   = entry ['DDDBtag'  ]      ## DDDB-tag
    ....  conddbtag = entry ['CondDBtag']      ## SIMCONDDB-tag 

    The obtained path in bookkeeping database can be used for subsequent BKQuery command:

    >>> data = BKQuery( path ).getDataset()

    It is a bit better top rely on ``ProductionPath'' :
    
    >>> data = BKQuery( entry['ProductionPath'] , Type = 'Production').getDataset()

    More complete list of cofiguration parameters for DaVinci-bases applicaitions
    can be accessed via DaVinciConf dictionary

    >>> conf = entry['DaVinciConf']
    >>> print conf.keys()

    The tags can be transferred to the application in application-dependent way:
    
    - For Bender-based applications, no need to deal with separate files,
    assuming that ``configure''-method in the Bender module treats 
    appropriately the argument ``params''

    >>> conf = entry['DaVinciConf']  ## get configuration
    >>> job.application.params = conf ## move configuration parameters to Bender
    
    - For GaudiExec/DaVinci-application one can construct some additional options file:

    >>> from tempfile import NamedTemporaryFile 
    >>> tag_file = NamedTemporaryFile ( delete = False , suffix = '.py')
    >>> tag_file.write('''
    ... from Configurables import DaVinci
    ... dv = DaVinci ( Simulation = True ,
    ...                DDDBtag    = '%s' ,
    ...                CondDBtag  = '%s' )
    ... ''' % ( dddbtag , conddbtab ) )
    >>> tag_file.close()
    >>> job.application.options.append ( tag_file.name ) 

    or, alternatively, with more predefined  parameters:

    >>> config   = entry['DaVinciConf'] 
    >>> from tempfile import NamedTemporaryFile 
    >>> tag_file = NamedTemporaryFile ( delete = False , suffix = '.py')
    >>> tag_file.write('''
    ... config = %s 
    ... from Configurables import DaVinci
    ... dv     = DaVinci ( **config ) 
    ... ''')
    >>> tag_file.close()
    >>> job.application.options.append ( tag_file.name ) 
    
    - A helper function ``daVinciMCConf'' is provided for GaudiExec/DaVinci application:

    >>> config   = entry['DaVinciConf']
    >>> the_file = daVinciMCConf ( **config ) 
    >>> job.application.options.append ( the_file ) 
    
    - Alternatively  one can use rely on ``extaOpts'' of GaudiExec:
    
    >>> options  = entry['DaVinciExtraOpts']
    >>> job.application.extraOpts = options 
    
    """
    ##
    import os 
    from subprocess import Popen, PIPE

    try :
        arguments = [ 'lb-run'                        ,
                      'LHCbDirac/prod'                ,
                      'dirac-bookkeeping-decays-path' ,
                      str(evttype)
                      ]
        pipe = Popen ( arguments           ,
                       env    = os.environ ,
                       stdout = PIPE       )
        
    except OSError :
        # most likely dirac script is not in the PATH!
        raise  

    ## case-insensitive  dictionary 
    #  @see https://stackoverflow.com/questions/2082152/case-insensitive-dictionary/32888599#32888599
    class CIDict(dict):
        """case-insensitive  dictionary
        - see https://stackoverflow.com/questions/2082152/case-insensitive-dictionary/32888599#32888599
        """        
        @classmethod
        def _k(cls, key):
            return key.lower() if isinstance(key, basestring) else key
        def __init__(self, *args, **kwargs):
            super(CIDict, self).__init__(*args, **kwargs)
            self._convert_keys()
        def __getitem__(self, key):
            return super(CIDict, self).__getitem__(self.__class__._k(key))
        def __setitem__(self, key, value):
            super(CIDict, self).__setitem__(self.__class__._k(key), value)
        def __delitem__(self, key):
            return super(CIDict, self).__delitem__(self.__class__._k(key))
        def __contains__(self, key):
            return super(CIDict, self).__contains__(self.__class__._k(key))
        def has_key(self, key):
            return super(CIDict, self).has_key(self.__class__._k(key))
        def pop(self, key, *args, **kwargs):
            return super(CIDict, self).pop(self.__class__._k(key), *args, **kwargs)
        def get(self, key, *args, **kwargs):
            return super(CIDict, self).get(self.__class__._k(key), *args, **kwargs)
        def setdefault(self, key, *args, **kwargs):
            return super(CIDict, self).setdefault(self.__class__._k(key), *args, **kwargs)
        def update(self, E={}, **F):
            super(CIDict, self).update(self.__class__(E))
            super(CIDict, self).update(self.__class__(**F))
        def _convert_keys(self):
            for k in list(self.keys()):
                v = super(CIDict, self).pop(k)
                self.__setitem__(k, v)
                
    result = [] 
    
    stdout = pipe.stdout
    for line in stdout :

        try : 
            value = eval ( line )
        except :
            continue
        
        if not isinstance ( value    , tuple ) : continue
        if not isinstance ( value[0] , str   ) : continue
        if not isinstance ( value[1] , str   ) : continue
        if not isinstance ( value[2] , str   ) : continue
        if not isinstance ( value[3] , int   ) : continue
        if not isinstance ( value[4] , int   ) : continue
        if not isinstance ( value[5] , int   ) : continue

        
        path       = value [0]
        production = value [5]
        
        ## adjust buggy path for GAUSSHIST 
        ## if path.endswith('/GAUSSHIST') :
        ##    nl = path.find('\n')
        ##    if 0 <= nl : path = path[nl:]
            
        ## skip GAUSSHIST 
        if path.endswith('/GAUSSHIST') : continue 
        
        ## create the entry: 
        entry = CIDict()

        entry [ 'path'         ] = value [0]
        entry [ 'DDDBtag'      ] = value [1]
        entry [ 'CondDBtag'    ] = value [2]
        entry [ 'NumFiles'     ] = value [3]
        entry [ 'NumEvents'    ] = value [4]
        
        entry [ 'EventType'    ] = evttype
        entry [ 'ProductionID' ] = production
        entry [ 'Production'   ] = production

        if   'MagDown' in path : entry [ 'Magnet' ] = 'MagDown'
        elif 'MagUp'   in path : entry [ 'Magnet' ] = 'MagUp'

        spath =  path.split('/')

        for s in spath[3:] :    
            su = s.upper()
            if   su.startswith ( 'SIM'      ) : entry [ 'Simulation' ] = s
            elif su.startswith ( 'TRIG0X'   ) : entry [ 'TriggerTCK' ] = s[4:]
            elif su.startswith ( 'STRIPPING') : entry [ 'Stripping'  ] = s
            elif su.startswith ( 'RECO'     ) : entry [ 'Reco'       ] = s
            elif su.startswith ( 'TURBO'    ) : entry [ 'Turbo'      ] = s

        entry ['FileType'  ] = spath[-1]
        
        upath = path.upper() 
        if   upath.startswith ( '/MC/2011'    ) : entry [ 'DataType' ] = '2011'
        elif upath.startswith ( '/MC/2012'    ) : entry [ 'DataType' ] = '2012'
        elif upath.startswith ( '/MC/2015'    ) : entry [ 'DataType' ] = '2015'
        elif upath.startswith ( '/MC/2016'    ) : entry [ 'DataType' ] = '2016'
        elif upath.startswith ( '/MC/2017'    ) : entry [ 'DataType' ] = '2017'
        elif upath.startswith ( '/MC/UPGRADE' ) : entry [ 'DataType' ] = 'Upgrade'

        if  entry.has_key('DataType') :
            datatype = entry['DataType']
            try :
                if str ( int ( datatype ) ) ==  datatype : entry['Year'] = datatype
            except :
                pass            
            
        if   upath.endswith ( '.DST'  ) : entry ['InputType'] = 'DST'
        elif upath.endswith ( '.LDST' ) : entry ['InputType'] = 'LDST'
        elif upath.endswith ( '.XDST' ) : entry ['InputType'] = 'XDST'
        elif upath.endswith ( '.MDST' ) : entry ['InputType'] = 'MDST'

        prod_path = spath[-1]
        if entry.has_key('Magnet'    ) : prod_path = '%s/%s'  % ( entry['Magnet'   ] , prod_path )
        if entry.has_key('DataType'  ) : prod_path = '%s/%s'  % ( entry['DataType' ] , prod_path )
        if entry.has_key('EventType' ) : prod_path = '%s/%s'  % ( entry['EventType'] , prod_path )
        entry ['productionpath'] = '/%s/%s' % ( production , prod_path )
        
        ##  prepare configuration for DaVinci 
        dv      = {}
        dv_keys = ( 'DDDBtag' , 'CondDBtag' , 'InputType' , 'DataType' )
        for k in dv_keys :
            if entry.has_key( k ) : dv[k] = entry[k] 

        dv [ 'Simulation' ] = True
        dv [ 'Lumi'       ] = False
        if upath.endswith ( 'ALLSTREAMS.MDST' ) : dv [ 'RootInTES' ] = '/Event/AllStreams'
        
        entry  ['DaVinciConf' ] = dv

        entry  ['DaVinciExtraOpts' ]  = '''
        \nfrom Configurables import DaVinci
        \nconfig = %s 
        \ndv     = DaVinci ( **config ) 
        '''  % dv  

        result.append ( entry )

    ## sorting: year, magnet, production 
    def  _kcmp ( o ) :
        try : 
            k = o['productionpath'][1:].split('/')
            return k[2],k[3],k[0]
        except :
            return o['production'] 
    
    ## sorting: year, magnet, production 
    result.sort ( key =  _kcmp )
    
    return tuple(result)




#def merge_root_output(job_id, ttree_name,output_filename)
#"""Merge all ROOT files from a job.
#
#Example usage:
#	merge_root_output(123,'D0ToKpi/DecayTree','/tmpt/f{0}.root'.format{job_id})
#
#"""
#	access_urls = [] #list of files we want to merge
#	for sj in jobs(job_id).subjobs.select(status='completed'):
#		for df in sj.outputfiles.get(DiracFile):
#			access_urls += df.accessURL() #mel by vyhodit chybu
#	import ROOT
#	tchain = ROOT.Tchain(ttree_name)
#	for url in access_urls:
#		tchain.Add(url)
#	tchain.Merge(output_filename)
