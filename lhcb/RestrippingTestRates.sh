#!/bin/bash
source /cvmfs/lhcb.cern.ch/group_login.sh;

E_NO_ARGS=65
if [ $# -eq 2 ]  # Must have two command-line args to demo script.
then
   	year=$1	
	new=$2
	if [ $new = true ] 
	then
	   	echo "Testing NEW lines for $year"
	   	read -p "Press enter to continue"
	else
		echo  "Testing OLD lines for $year"
		read -p "Press enter to continue"
	
	fi
else
  	echo "Please invoke this script with two command-line arguments in the following format:"
	echo "./checkoutDV.sh YEAR NEW"
	exit $E_NO_ARGS
fi


if  [ "$year" = "2018" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/32
then
	export CMTCONFIG=x86_64-slc6-gcc62-opt
	LbLogin.sh	

	cd ./2018/S34r0p1/DaVinciDev_v44r7
	make purge
	make

	cd build.x86_64-slc6-gcc62-opt

	if [ $new = true ] 
	then
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee ../../../../logFiles/B2XMuMu_2018_new.log
	else
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee ../../../../logFiles/B2XMuMu_2018_old.log
	fi
	
fi


if  [ "$year" = "2017" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/34
then
	export CMTCONFIG=x86_64-slc6-gcc62-opt
	LbLogin.sh	

	cd 2017/S29r2p1/DaVinciDev_v42r8p1

	make purge
	make

	cd build.x86_64-slc6-gcc62-opt

	if [ $new = true ] 
	then
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee ../../../../logFiles/B2XMuMu_2017_new.log
	else
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee ../../../../logFiles/B2XMuMu_2017_old.log
	fi
	
fi


if  [ "$year" = "2016" ]; #
then
	echo "Not available yet"
fi


if  [ "$year" = "2015" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/33
then
	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	cd 2015/S24r2/DaVinciDev_v38r1p7
	make purge
	make

	cd build.x86_64-slc6-gcc49-opt

	if [ $new = true ] 
	then
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee../../../../logFiles/B2XMuMu_2015_new.log
	else
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine.py | tee ../../../../logFiles/B2XMuMu_2015_old.log
	fi
	
fi


if  [ "$year" = "2012" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/31
then
	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	cd 2012/S21r1p2/DaVinciDev_v39r1p1
	make purge
	make

	cd build.x86_64-slc6-gcc49-opt

	if [ $new = true ]
	then
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine_Run1_B2XMuMu.py | tee ../../../../logFiles/B2XMuMu_2012_new.log
	else
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine_Run1_B2XMuMu.py | tee ../../../../logFiles/B2XMuMu_2012_old.log
	fi
	
fi



if  [ "$year" = "2011" ]; #https://gitlab.cern.ch/lhcb/Stripping/issues/31
then

	export CMTCONFIG=x86_64-slc6-gcc49-opt
	LbLogin.sh	

	cd 2011/S21r0p2/DaVinciDev_v39r1p1
	make purge
	make

	cd build.x86_64-slc6-gcc49-opt

	if [ $new = true ]  
	then
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine_Run1_B2XMuMu.py | tee ../../../../logFiles/B2XMuMu_2011_new.log
	else
		./run gaudirun.py  -T ../Phys/StrippingSelections/tests/users/TestMyStrippingLine_Run1_B2XMuMu.py | tee ../../../../logFiles/B2XMuMu_2011_old.log
	fi
	
fi



