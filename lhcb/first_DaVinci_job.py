#run by lb-run DaVinci v40r2 ipython -i first.py 00035742_00000002_1.allstreams.dst
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2012'

# Pass file to open as first comman line argument
inputFiles = [sys.argv[-1]]
IOHelper('ROOT').inputFiles(inputFiles)

appMgr = GP.AppMgr()
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()

def nodes(evt, node=None):
    """List all nodes in `evt`"""
    nodenames = []

    if node is None:
        root = evt.retrieveObject('')
        node = root.registry()

    if node.object():
        nodenames.append(node.identifier())
        for l in evt.leaves(node):
            # skip a location that takes forever to load
            # XXX How to detect these automatically??
            if 'Swum' in l.identifier():
                continue

            temp = evt[l.identifier()]
            nodenames += nodes(evt, l)

    else:
        nodenames.append(node.identifier())

    return nodenames



def advance(decision):
    """Advance until stripping decision is true, returns
    number of events by which we advanced"""
    n = 0
    while True:
        appMgr.run(1)

        if not evt['/Event/Rec/Header']:
            print 'Reached end of input files'
            break

        n += 1
        dec=evt['/Event/Strip/Phys/DecReports']
        if dec.hasDecisionName('Stripping{0}Decision'.format(decision)):
            break

    return n

line = 'D2hhCompleteEventPromptDst2D2RSLine'
advance(line)
cands = evt['/Event/AllStreams/Phys/D2hhCompleteEventPromptDst2D2RSLine/Particles']
cand = cands[0]

from LoKiPhys.decorators import PT, MM, P, VCHI2, VFASPF
VCHI2(cand.endVertex()) == VFASPF(VCHI2)(cand)

pv_finder_tool = appMgr.toolsvc().create(
    'GenericParticle2PVRelator<_p2PVWithIPChi2, OfflineDistanceCalculatorName>/P2PVWithIPChi2',
    interface='IRelatedPVFinder'
)
pvs = evt['/Event/AllStreams/Rec/Vertex/Primary']
best_pv = pv_finder_tool.relatedPV(cand, pvs)
from LoKiPhys.decorators import DIRA


def find_tracks(particle):
    tracks = []
    if particle.isBasicParticle():
        proto = particle.proto()
        if proto:
            track = proto.track()
            if track:
                try:
                    tracks.append(particle.data())
                except:
                    tracks.append(particle)
    else:
        for child in particle.daughters():
            tracks.extend(find_tracks(child))
    return tracks

max_pt = max([PT(child) for child in find_tracks(cand)])

from LoKiPhys.decorators import MAXTREE, ISBASIC, HASTRACK
MAXTREE(ISBASIC & HASTRACK, PT, -1)(cand) == max_pt

from LoKiPhys.decorators import SUMTREE, ABSID
print SUMTREE(211 == ABSID, PT)(cand)
print SUMTREE('pi+' == ABSID, PT)(cand)


from LoKiPhys.decorators import CHILD
# Option 1
mass = MM(cand.daughtersVector()[0])
# Option 2
mass_child = CHILD(MM, 1)(cand)
# Do they agree?
mass == mass_child

#rychlejsi zpusob cutu
from LoKiCore.functions import in_range
in_range(2000, MM, 2014)(cand)
in_range(1860, CHILD(MM, 1), 1870)(cand)

#LoKi provides wrapper functors that evaluate a functor (or functor expression), print debugging information and return the result; the most important of these are:

    dump1, which prints the input object and returns the calculated functor value,

    from LoKiCore.functions import dump1
    debug_p_components_sum = dump1(p_components_sum)
    debug_p_components_sum(cand)

    monitor which prints the input the functor string and returns the calculated functor value,

    from LoKiCore.functions import monitor
    monitor_p_components_sum = monitor(p_components_sum)
    monitor_p_components_sum(cand)
		
