
import subprocess
from subprocess import Popen, PIPE
import numpy as np

decay_ID_dict = {
    "SigMC" :    12113100, 
    "RefMC" :    12143401, 
    "PHSP" :     12113446,
    "Inclusive": 12442001
}

#ONLY FOR MC!

def parse_bkk_path(bkk_path):
    bkk_path = bkk_path.split('/')[1:] #bkk_path[0] is '('
    #parse also the Beam4000GeV-2012-MagDown-Nu2.5-Pythia8 path
    path_list = bkk_path[2].split('-')
    
    if ('Digi' in bkk_path[4]): bkk_path = np.delete(bkk_path,4)
    if ('25ns' in path_list): path_list = np.delete(path_list,4)

    if ('Upgrade' in path_list): return {
                'data' :   bkk_path[0],
                'year':   bkk_path[1],
                'E':      path_list[0],
                'pol':    path_list[2],
                'nu':     path_list[3],
                'pythia': path_list[4],
                'simVer': bkk_path[3],
                'decID':  bkk_path[4],
                'file':   bkk_path[5],
    }
    return  { 'data' :   bkk_path[0],
               'year':   bkk_path[1],
               'E':      path_list[0],
               'pol':    path_list[2],
               'nu':     path_list[3],
               'pythia': path_list[4],
               'simVer': bkk_path[3],
               'trig':   bkk_path[4],
               'reco':   bkk_path[5],
               'strip':  bkk_path[6],
               'decID':  bkk_path[7],
               'file':   bkk_path[8],
            }

def parse_bkk_decays_path(bkk_list):
    new_dict = {
        'dddb'  :     bkk_list[1],
        'condb' :     bkk_list[2],
        'nFiles':     bkk_list[3],
        'nEvents':    bkk_list[4],
        'prodID':     bkk_list[5].replace(")","")
    }
    path_dict = parse_bkk_path(bkk_list[0])
    if not (path_dict): return {''}
    final_dict = {**path_dict, **new_dict}
    return final_dict

def get_parsed_bkk_decays_path(decayID):
    #Before runnig on lxplus, run: lb-conda default!
    #Call Dirac
    dirac_command = subprocess.Popen('/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/1020/stable/linux-64/bin/lb-dirac dirac-bookkeeping-decays-path {0}'.format(decayID), shell=True, stdout=subprocess.PIPE)	
    #Read in Dirac output
    stdout = dirac_command.communicate()[0]
    #Split the output per line
    stdout = stdout.decode().split('\n') 

    list_of_dicts = []
    for sample in stdout:
        #Split into a list of path,dddb, sim, nFiles, nEvents, production ID
        tmpList = sample.split(',')
        if (len(tmpList)==1): continue #Remove empty answers
        list_of_dicts.append(parse_bkk_decays_path(tmpList))
    #print (list_of_dicts)
    return list_of_dicts    

def print_NoEvents(decayID):       
    for sample in get_parsed_bkk_decays_path(decayID):    
        print (sample.get('year'),sample.get('pol'),'\t',sample.get('simVer'),sample.get('nEvents'))
    
def print_conditions(decayID):       
    for sample in get_parsed_bkk_decays_path(decayID):    
        print (sample.get('year'),sample.get('pol'),'\t',sample.get('simVer'),sample.get('dddb'),sample.get('condb'))
    
for ID in decay_ID_dict.keys():
    print ('\n\t',ID)
    print_NoEvents(decay_ID_dict.get(ID))
