



decay_number_dict = {
    "SigMC" : { 
        "2011": {
            "E": "3500",
            "nu": "2",
            "trigger": "0x40760037",
            "stripping": "21r1",
            "reco": "Reco14c",
            "simVer": "Sim09a",
            "dst": "DST",
            "merge": ""
        },
        "2012": {
            "E": "4000",
            "nu": "2.5",
            "trigger": "0x409f0045",
            "stripping": "21",
            "reco": "Reco14c",
            "simVer": "Sim09a",
            "dst": "DST",
            "merge": ""
        },
        "2015": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x411400a2",
            "stripping": "24",
            "reco":  "Reco15a/Turbo02",
            "simVer": "Sim09b",
            "dst": "MDST",
            "merge": ""
        },
        "2016": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x6138160F",
            "stripping": "28",
            "reco":  "Reco16/Turbo03",
            "simVer": "Sim09e",
            "dst": "MDST",
            "merge": ""
        },
        "2017": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x62661709",
            "stripping": "29r2",
            "reco":  "Reco17/Turbo04a-WithTurcal",
            "simVer": "Sim09e",
            "dst": "MDST",
            "merge": ""
        },
        "2018": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x617d18a4",
            "stripping": "34r0p1", #34 for 12115102
            "reco":  "Reco18/Turbo05-WithTurcal", 
            "simVer": "Sim09h", #Sim09f for 12115102
            "dst": "DST",
            "merge": ""
        },
        "general":{
            "dec_ID": "12113100", #same for 12115102
            "filter": "NoPrescalingFlagged",
            "stream": "ALLSTREAMS"
        }    
    },
    "PHSP" : {  #same for 12115179
        "2011": {
            "E": "3500",
            "nu": "2",
            "trigger": "0x40760037",
            "stripping": "21r1p1",
            "reco": "Reco14c",
            "simVer": "Sim09f",
            "dst": "DST",
            "merge": "Merge14/"
        },
        "2012": {
            "E": "4000",
            "nu": "2.5",
            "trigger": "0x409f0045",
            "stripping": "21r0p1",
            "reco": "Reco14c",
            "simVer": "Sim09f",
            "dst": "DST",
            "merge": "Merge14/"
        },
        "2015": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x411400a2",
            "stripping": "24r1",
            "reco":  "Reco15a/Turbo02",
            "simVer": "Sim09h",
            "dst": "MDST",
            "merge": ""
        },
        "2016": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x6138160F",
            "stripping": "28r1",
            "reco":  "Reco16/Turbo03",
            "simVer": "Sim09f",
            "dst": "MDST",
            "merge": "Merge14/"
        },
        "2017": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x62661709",
            "stripping": "29r2",
            "reco":  "Reco17/Turbo04a-WithTurcal",
            "simVer": "Sim09f",
            "dst": "MDST",
            "merge": "Merge14/"
        },
        "2018": {
            "E": "6500",
            "nu": "1.6-25ns",
            "trigger": "0x617d18a4",
            "stripping": "34",
            "reco":  "Reco18/Turbo05-WithTurcal",
            "simVer": "Sim09f",
            "dst": "MDST",
            "merge": "Merge14/"
        },
        "general":{
            "dec_ID": "12113446", #same for 12115179
            "filter": "Filtered",
            "stream": "B2XMUMU.STRIP"
        }
    }
}

def printPath(year, pol, sample):
    tmp_dict = dict(decay_number_dict[sample][year])
    gen_dict = dict(decay_number_dict[sample]["general"])
    tmp_dict.update(gen_dict)

    tmp_dict["year"] = year
    tmp_dict["pol"] = pol
    print tmp_dict
           
    PATH = "/MC/%(year)s/Beam%(E)sGeV-%(year)s-Mag%(pol)s-Nu%(nu)s-Pythia8/%(simVer)s/Trig%(trigger)s/%(reco)s/Stripping%(stripping)s%(filter)s/%(merge)s%(dec_ID)s/%(stream)s.%(dst)s" % tmp_dict
    print ('Using data: ', PATH)
    return PATH
        
