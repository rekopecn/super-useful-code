from Configurables import DaVinci
from GaudiConf import IOHelper

DaVinci().InputType = 'DST' #creating an instance of DaVinci
                            #muzu udelat dv = DaVinci()
                            #            dv1=DaVinci()
                            # pak dv == dv1


from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons  as Kaons

d0_daughters ={'pi-': 'PT > 750*MeV',
                'K+': 'PT > 750*MeV'
                }
d0_comb = "(AMAXDOCA('') < 0.2*mm) & (ADAMASS('D0')<100*MeV)"               #Doca needs this ('') thing

d0_mother = "(VFASPF(VCHI2/VDOF) < 9) & (ADMASS('D0')<70*MeV)"

from Configurables import CombineParticles #dava dohrommady castice

d0 = CombineParticles('Combine_D0',
                        DecayDescriptor='[D0 -> pi- K+]cc',
                        DaughtersCuts = d0_daughters, #bacha na to, daughter je vic; takze cuts a ne cut
                        CombinationCut=d0_comb,
                        MotherCut=d0_mother)

#seleciton wraps the algorithm, handles in/output
from PhysConf.Selections import Selection
###### pouzivat python a dat help(Selection)
d0_sel = Selection('Sel_D0',
                    Algorithm=d0,
                    RequiredSelections=[Pions,Kaons])

dstar_daughters = {'pi+':'TRCHI2DOF<3'}
dstar_comb = "ADAMASS('D*(2010)+') < 400*MeV"
dstar_mother ="(abs(M-MAXTREE('D0'==ABSID,M))-145.42)<10*MeV"
decay_descriptor = '[D*(2010)+ -> D0 pi+]cc'

dstar = CombineParticles('Combine_D*',
                        DecayDescriptor=decay_descriptor,
                        DaughtersCuts = dstar_daughters,
                        CombinationCut = dstar_comb,
                        MotherCut=dstar_mother)

dstar_sel = Selection(
                    'Sel_Dstar',
                    Algorithm=dstar,
                    RequiredSelections=[d0_sel,Pions]
)

#try print selection

from PhysConf.Selections import SelectionSequence
dstar_seq = SelectionSequence('Dstar_seq',TopSelection=dstar_sel)
seq = dstar_seq.sequence()
dstar_seq.outputLocations()


dv = DaVinci()

#create an ntuple
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
dtt = DecayTreeTuple('TupleDstToD0pi_DstarToKpi')
##### FIXME
dtt.Inputs = dstar_seq.outputLocations()
dtt.Decay = '[D*(2010)+ -> D0 pi+]CC'

DaVinci().UserAlgorithms += [dstar_seq.sequence(),dtt]


dv.TupleFile = 'tuple.root'
dv.PrintFreq = 100
dv.DataType = '2012'
dv.Simulation = True
dv.EvtMax = 100

IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035742/0000/00035742_00000001_1.allstreams.dst')], clear=True)
