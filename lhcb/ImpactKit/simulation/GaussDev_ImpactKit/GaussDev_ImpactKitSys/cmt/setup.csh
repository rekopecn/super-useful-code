# echo "Setting GaussDev_ImpactKitSys HEAD in /afs/cern.ch/user/r/rekopecn/ImpactKit/simulation/GaussDev_ImpactKit"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=GaussDev_ImpactKitSys -version=HEAD -path=/afs/cern.ch/user/r/rekopecn/ImpactKit/simulation/GaussDev_ImpactKit  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

