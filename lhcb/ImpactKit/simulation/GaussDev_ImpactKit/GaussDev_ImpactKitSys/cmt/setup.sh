# echo "Setting GaussDev_ImpactKitSys HEAD in /afs/cern.ch/user/r/rekopecn/ImpactKit/simulation/GaussDev_ImpactKit"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=GaussDev_ImpactKitSys -version=HEAD -path=/afs/cern.ch/user/r/rekopecn/ImpactKit/simulation/GaussDev_ImpactKit  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

