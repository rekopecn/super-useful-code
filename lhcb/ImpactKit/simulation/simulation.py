#Questions. replace pythia with something, how?
from Gauss.Configuration import GenInit

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1   #tyhle dve radky jsou
GaussGen.RunNumber = 1082       #vlastne seedy pro random generator

from Configurables import LHCbApp
LHCbApp().DDDBtag = 'dddb-20150724'
LHCbApp().CondDBtag = 'sim-20160623-vc-md100'
LHCbApp().EvtMax = 5
