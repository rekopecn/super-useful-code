from Configurables import Moore

# Define settings
Moore().ThresholdSettings = "Physics_pp_2017" #settings for 2017 pp datataking
Moore().RemoveInputHltRawBanks = True #
Moore().Split = '' #configuring both HLT1 and HLT2 lines
#takze pro configuraci jenom HLT1 bude
#Moore().Split = 'HLT1'
#pro configuraci jenom HLT2 bude
#Moore().RemoveInputHltRawBanks = False # Why?
#Moore().Split = 'Hlt2'
#....
#TestFileDB.test_file_db["2016_Hlt1_0x11361609"].run(configurable=Moore())
#v principu aby HLT2 vedelo, co delalo HLT1

# A bit more output
from Gaudi.Configuration import INFO
Moore().EnableTimer = True
Moore().OutputLevel = INFO

# Input data
from PRConfig import TestFileDB

# The following call configures input data, database tags and data type
TestFileDB.test_file_db["2016NB_25ns_L0Filt0x1609"].run(configurable=Moore())
Moore().DataType = "2017"

# Override the TCK in the ThresholdSettings to match the input data
from Configurables import HltConf
HltConf().setProp("L0TCK", '0x1609')

# Remove a line which accepts every event when run on this sample
HltConf().RemoveHlt1Lines = ["Hlt1MBNoBias"]

Moore().EvtMax = 100
print Moore()
