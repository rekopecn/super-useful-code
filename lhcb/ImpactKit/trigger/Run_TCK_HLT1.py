from Configurables import Moore
# Define settings
Moore().UseTCK = True
# You can check in TCKsh which TCKs exist and for which Moore versions they can be used.
#first 4 digits is HLT, last 4 digit L0
#TCK starting with 1 je pro HLT1, starting with 2 je pro HLT2
Moore().InitialTCK = "0x11381609"
Moore().Split = 'Hlt1'
Moore().RemoveInputHltRawBanks = True
# In the online farm Moore checks if the TCK in data and the configuration are the same.
# Here we disable it as we run a different TCK.
Moore().CheckOdin = False

#nastavuje outputfile
Moore().outputFile = "TestTCK1.mdf"
# A bit more output
from Gaudi.Configuration import INFO
Moore().EnableTimer = True
Moore().OutputLevel = INFO
# Input data
from PRConfig import TestFileDB
TestFileDB.test_file_db["2016NB_25ns_L0Filt0x1609"].run(configurable=Moore())
Moore().DataType = "2016"
Moore().EvtMax = 100
print Moore()
