from Configurables import Moore
# Define settings
Moore().UseTCK = True
Moore().InitialTCK = "0x21381609"
Moore().DataType = "2016"
Moore().Split = 'Hlt2'
Moore().RemoveInputHltRawBanks = False
Moore().CheckOdin = False
Moore().EnableOutputStreaming = True #vytvoří asi pet fajlu s ruznema streamama
Moore().outputFile = "TestTCK2.mdf"
# A bit more output
from Gaudi.Configuration import INFO
Moore().EnableTimer = True
Moore().OutputLevel = INFO
# Input data
#from PRConfig import TestFileDB
Moore().DDDBtag = 'dddb-20150724'
Moore().CondDBtag = 'cond-20170325'
Moore().inputFiles = ["TestTCK1.mdf"]
Moore().EvtMax = 100

from Configurables import EventClockSvc
EventClockSvc( InitialTime = 1465516800000000000 ) # 16th June 2016
