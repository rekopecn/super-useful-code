from Configurables import Moore
# Define settings
Moore().UseTCK = True
Moore().InitialTCK = "0x11381609"
Moore().DataType = "2016"
Moore().Split = 'Hlt1'
Moore().RemoveInputHltRawBanks = True
Moore().CheckOdin = False
Moore().outputFile = "TestTCK1.mdf"
# A bit more output
Moore().EnableTimer = True
Moore().OutputLevel = 4
# Input data
from PRConfig import TestFileDB
TestFileDB.test_file_db["2016NB_25ns_L0Filt0x1609"].run(configurable=Moore())
Moore().EvtMax = 1000
