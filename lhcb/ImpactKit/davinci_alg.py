from Configurables import DaVinci, SubstitutePID
from GaudiConf import IOHelper
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

#stream and stripping line we want to Use

stream = 'AllStreams'
line = 'D2hhCompleteEventPromptDst2D2RSLine'
testLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream,line)

#get the selections(s) created by the stripping
strippingSels=[DataOnDemand(Location=testLoc)]

#####
subs = SubstitutePID("MakeD02pipi",
                    Code="DECTREE('[D*(2010)+ -> (D0 -> K- pi+) pi+]CC')",  #bere vsechno, co bylo matched by the stripping line
                    Substitutions={'Charm -> (D0 -> ^K- pi+) Meson': 'pi-',
                                        # tohle predela PID castic v tomhle rozpadu
                                        #nedela to rovnou charged cojugates....
                                    'Charm -> (D~0 -> ^K+ pi-) Meson': 'pi+'}
                    )
                    #dobry kdyz koukam na mismatched castice

selSub = Selection("Sel_masshypo",
                    Algorithm=subs,
                    RequiredSelections=strippingSels #uz je list, see l.14
                    )

selSeq = SelectionSequence("selSeq",TopSelection=selSub)

####


dtt = DecayTreeTuple('TupleDstToD0pi_D0Topipi')
dtt.Inputs = [selSeq.outputLocation()]
dtt.Decay = '[D*(2010)+ -> ^(D0 -> ^pi- ^pi+) ^pi+]CC'

seq = GaudiSequencer('MyTupleSeq')
seq.Members +=[selSeq.sequence()]
seq.Members +=[dtt]

dv = DaVinci()
dv.appendToMainSequence([seq])

dv.InputType = 'DST'
dv.TupleFile = 'tuple_alg.root'
dv.PrintFreq = 100
dv.DataType = '2012'
dv.Simulation = True
dv.EvtMax = 1000

IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035742/0000/00035742_00000001_1.allstreams.dst')], clear=True)
