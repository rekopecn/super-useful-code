from Configurables import DaVinci, SubstitutePID, FilterInTrees, CombineParticles
from GaudiConf import IOHelper
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

from StandardParticles import StdAllNoPIDsPions as Pions
#stream and stripping line we want to Use

stream = 'AllStreams'
line = 'D2hhCompleteEventPromptDst2D2RSLine'
testLoc = '/Event/{0}/Phys/{1}/Particles'.format(stream,line)

#get the selections(s) created by the stripping
strippingSels=[DataOnDemand(Location=testLoc)]

################################################

D0_from_dst = FilterInTrees("fil_D0_from_dst",Code = "DECTREE('D0 -> K- pi+')CC")

selD0 = Selection("SelD0",
                    Algorithm = D0_from_dst,
                    RequiredSelections=strippingSels)
                        #nevybira D0 od D*
                        #ale chceme koukat na kaon a co se stane, kdyz to sparujem s pi
                        #pac to nemusi byt pi z D0
                        #tohle je potreba udelat pro kazdou rezonanci

K_filter = FilterInTrees("K_filter",Code = "(ABSID=='K-')")
                        #probehne to pres vsechny vybrany castice
                        #a zkontroluje, ze kazda castice je kaon
                        #aka vybirame jenom kaony

K_from_d0 = Selection("selK",
                        Algorithm=K_filter,
                        RequiredSelections=[selD0])
                        #

Kst = CombineParticles("K_star_CP",
                        DecayDescriptor = ['K*(892)0 -> K- pi+'],
                        CombinationCut = "(ADAMASS('K*(892)0')<350*MeV)",
                        MotherCut="VSASPF(VCHI2/VDOF) < 9")
#FIXME
Kst_sel = Selection("SelKst",
                    Algorithm=Kst,
                    RequiredSelections = [K_from_d0,Pions]
                    )

#################################################


selSeq = SelectionSequence("selSeq",TopSelection=Kst_sel)




dtt = DecayTreeTuple('TupleDstToD0pi_D0Topipi')
dtt.Inputs = [selSeq.outputLocation()]
dtt.Decay = '[D*(2010)+ -> ^(D0 -> ^pi- ^pi+) ^pi+]CC'

seq = GaudiSequencer('MyTupleSeq')
seq.Members +=[selSeq.sequence()]
seq.Members +=[dtt]

dv = DaVinci()
dv.appendToMainSequence([seq])

dv.InputType = 'DST'
dv.TupleFile = 'tuple_alg.root'
dv.PrintFreq = 100
dv.DataType = '2012'
dv.Simulation = True
dv.EvtMax = 1000

IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035742/0000/00035742_00000001_1.allstreams.dst')], clear=True)
