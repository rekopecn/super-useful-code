from Configurables import DaVinci
from GaudiConf import IOHelper
from Configurables import DecayTreeTuple



from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseKaons  as Kaons


#D0 cuts
d0_daughters ={'pi-': 'PT > 750*MeV',
                'K+': 'PT > 750*MeV'
                }
d0_comb = "(AMAXDOCA('') < 0.2*mm) & (ADAMASS('D0')<100*MeV)"               #Doca needs this ('') thing

d0_mother = "(VFASPF(VCHI2/VDOF) < 9) & (ADMASS('D0')<70*MeV)"


#from PhysConf.Selections import SimpleSelection
#from GaudiConfUtils.ConfigurableGenerators as C
        ###NEEDS TO BE THERE TO GIVE THE CombineParticles A NAME
        ### PRETY FUGLY

        #d0_sel = SimpleSelection("Sel_D0",
        #                         C.CombineParticles,
        #                         [Pions,Kaons],
        #                         DecayDescriptor="[D0 -> pi-K+]cc",
        #                         DaughtersCuts = d0_daughters,
        #                         CombinationCut=d0_comb,
        #                         MotherCut=d0_mother)

#misto toho vezmeme filter desktop, kterej je stejne otravnej
#from Configurables import FilterDesktop
#cut = 'PT > 1000*MeV'
#filter_ = FilterDesktop('MyFilter',Code=cut)
#FilteredPions = Selection("MyFilter_Sel",
#                            Algorithm=filter_,
#                            RequiredSelections=[Pions])
#from PhysConf.Selections import FilterSelection
#FilteredPions = FilterSelection("MyFilter_Sel",
#                                    [Pions],
#                                    Code=cut)
#d0_sel = CombineSelection("Sel_D0",
#                            [FilteredPions]),
#                            )

from PhysConf.Selections import CombineSelection
d0_sel = CombineSelection("Sel_D0",
                            [Pions,Kaons],
                            DecayDescriptor='[D0 -> pi- K+]cc',
                            DaughtersCuts = d0_daughters,
                            CombinationCut=d0_comb,
                            MotherCut=d0_mother)


from PhysConf.Selections import SelectionSequence
dzero_seq = SelectionSequence('Dzero_seq',TopSelection=d0_sel)
seq = dzero_seq.sequence()
dzero_seq.outputLocations()

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
dtt = DecayTreeTuple('TupleDstToD0pi_DstarToKpi')
dtt.Inputs = dzero_seq.outputLocations()
dtt.Decay = '[D0 -> pi- K+]CC'

#Here, have a desperate solution, very very verbose
from Configurables import StoreExplorerAlg


DaVinci().UserAlgorithms += [StoreExplorerAlg('Before'),
                            dzero_seq.sequence(),
                            dtt,
                            StoreExplorerAlg('After')]

#DaVinci config
dv = DaVinci()
dv.InputType = 'DST'
dv.TupleFile = 'tuple_modern.root'
dv.PrintFreq = 100
#dv.PrintSelection = True ???? tak nejak to asi vypada
dv.DataType = '2012'
dv.Simulation = True
dv.EvtMax = 1000

IOHelper().inputFiles([('root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00035742/0000/00035742_00000001_1.allstreams.dst')], clear=True)


#output zaroven na screenu a ve file: echo "hi" | tee log
