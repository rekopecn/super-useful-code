#TASK: chceme gangajob file, kterymu dame  input
import argparse
import sys

print sys.argv

parser = argparse.ArgumentParser(
    description='Make a DaVinci job'
)
parser.add_argument('polarity', #staci jenom jmeno, zbytek je otpional
                        choices = ['MagUp','MagDown'], #konretni moznosti
                        help='Polarity of the magnet')
parser.add_argument('year',
                        type=  int, #specifikace konretniho typu
                        choices = [2011,2012,2015,2016],
                        help='Year of data-taking to process, integer')
                        #bere, ze oba jsou mandatory
parser.add_argument('--test',
                        action = 'store_true',
                        help = 'Run locally over a single file')
args = parser.parse_args()

#'ulozime' input
year = args.year
test = args.test
polarity = args.polarity
if test:
    print '/LHCb/{year}/{polarity}/../CHARM.MDST'.format(year=year, polarity=polarity)

#bkq = BKQuery('/LHCb/{year}/{polarity}/../CHARM.MDST'.format(year=year, polarity=polarity))

#make job

#load data
